package com.bd._main.GUI;

import java.io.*;
import java.nio.channels.*;

import javax.swing.*;

public class LockApp {

	public static Boolean lockInstance(final String lockFile) {
	    try {
	        final File file = new File(lockFile);
	        final RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
	        final FileLock fileLock = randomAccessFile.getChannel().tryLock();
	        if (fileLock != null) {
	            Runtime.getRuntime().addShutdownHook(new Thread() {
	                public void run() {
	                    try {
	                        fileLock.release();
	                        randomAccessFile.close();
	                        file.delete();
	                    } catch (Exception e) {
	                    	JOptionPane.showMessageDialog(null, "Unable to remove lock file: " + lockFile, 
	            					"Invalid User Credentials", JOptionPane.ERROR_MESSAGE);
//	                    	log.error("Unable to remove lock file: " + lockFile, e);
	                    }
	                }
	            });
	            return true;
	        }
	    } catch (Exception e) {
	    	JOptionPane.showMessageDialog(null, "Unable to create and/or lock file: " + lockFile, 
					"Invalid User Credentials", JOptionPane.ERROR_MESSAGE);
//	    	log.error("Unable to create and/or lock file: " + lockFile, e);
	    }
	    return false;
	}
}
