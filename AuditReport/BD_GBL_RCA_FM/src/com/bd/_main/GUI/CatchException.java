package com.bd._main.GUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class CatchException extends JDialog {

	private static final long serialVersionUID = 1L;
	private static JLabel lblException = new JLabel();
	private static JTextArea txtExceptionDetails = new JTextArea();
	public static Boolean cancelledJob = false; 
	private final JScrollPane scrollPane = new JScrollPane();
	private final JPanel panelHeaders = new JPanel();
	private final JPanel panelDetails = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			CatchException testExceptionWindow = new CatchException("TEST");
			testExceptionWindow.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			testExceptionWindow.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Create the dialog.
	 */
	public CatchException(String textException) {
		setTitle("Exception Thrown...");
		setModal(true);
		setBounds(100, 100, 400, 300);
		getContentPane().setLayout(new BorderLayout());
		setLocationRelativeTo(null);
		
		getContentPane().add(panelHeaders, BorderLayout.NORTH);
		panelHeaders.add(lblException);
		lblException.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblException.setText("Unhandled Exception Caught. Please see details below.");
		
		JButton btnClose = new JButton("Close");
		panelHeaders.add(btnClose);
		
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});  
		
        panelDetails.setBounds(0, 0, 384, 212);
        getContentPane().add(panelDetails, BorderLayout.CENTER);
        panelDetails.setLayout(new BorderLayout(0, 0));
        
        panelDetails.add(scrollPane);
        scrollPane.setViewportView(txtExceptionDetails);

        txtExceptionDetails.setFont(new Font("Tahoma", Font.PLAIN, 12));      
        txtExceptionDetails.setText(textException);
        txtExceptionDetails.setBorder(UIManager.getBorder("TextField.border"));
        txtExceptionDetails.setEditable(false);	
	}
}