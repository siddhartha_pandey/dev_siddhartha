package com.bd._main.GUI;

import javax.swing.*;

import com.bd.common.util.*;

public class UsersModel {
	private DefaultListModel<String> usersListModel;

	public UsersModel() {
		usersListModel = new DefaultListModel<>();
	    for(int i=0; i < CommonVariables.usersList.size(); i++) {
	    	usersListModel.add(i, CommonVariables.usersList.get(i));
	    }
	}

	public ListModel<String> getListModel() {
		return usersListModel;
	}
	
	
}
