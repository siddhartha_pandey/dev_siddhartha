package com.bd._main.GUI;

import javax.swing.*;
import javax.swing.filechooser.*;

public class SelectFile {
	
	public static String getInputFile ()
	{
		String selectedFile = null;
		JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		jfc.setAcceptAllFileFilterUsed(false);
		jfc.setDialogTitle("Choose an input Excel file to process: ");
		jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Excel file", "xlsx");
		jfc.setFileFilter(filter);
	
		int returnValue = jfc.showOpenDialog(null);
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			if (jfc.getSelectedFile().isFile()) {
				selectedFile = jfc.getSelectedFile().toString().trim();
			}
		}
		else if (returnValue == JFileChooser.CANCEL_OPTION){
			//do nothing
		}
		else{
			System.out.println("Error on Directory SDialog Window");
		}
		return selectedFile;
	}
}
