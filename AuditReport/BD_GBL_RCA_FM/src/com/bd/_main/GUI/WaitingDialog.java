package com.bd._main.GUI;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;

@SuppressWarnings("serial")
public class WaitingDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			WaitingDialog dialog = new WaitingDialog("Cancelling...");
			dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public WaitingDialog(String activity) {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) { 
				//Do Nothing		
			}
		});
		
		setModal(true);
		setTitle("Please Wait");
		setBounds(100, 100, 190, 100);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		setLocationRelativeTo(null);
		
		JLabel lblNewLabel = new JLabel(activity);
		contentPanel.add(lblNewLabel);
		{
			JProgressBar progressBar = new JProgressBar();
			progressBar.setIndeterminate(true);
			contentPanel.add(progressBar);
		}
	}
}
