package com.bd._main.GUI;

import java.util.Collections;

import javax.swing.*;

import com.bd.common.util.*;

public class ObjectTypeModel {
	private DefaultComboBoxModel<String> objectTypeModel;

	public ObjectTypeModel() {
		Collections.sort(CommonVariables.listPropertiesAuditGroups);
		String [] listAuditObjects = CommonVariables.listPropertiesAuditGroups.toArray(new String[0]);
		objectTypeModel = new DefaultComboBoxModel<>(listAuditObjects);
	}

	public ComboBoxModel<String> getComboModel() {
		return objectTypeModel;
	}
}
