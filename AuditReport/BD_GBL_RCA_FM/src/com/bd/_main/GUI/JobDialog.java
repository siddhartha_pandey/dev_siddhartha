package com.bd._main.GUI;

import java.awt.*;
import java.awt.event.*;
import java.util.concurrent.*;
import javax.swing.*;
import javax.swing.event.*;

import com.bd.common.util.*;

@SuppressWarnings("serial")
public class JobDialog extends JDialog {

	public static JobDialog progressWindow = null;
	public JProgressBar progressBar = new JProgressBar(0, 100);
	private static JTextArea txtProgressDetails = new JTextArea();
	private static JButton btnProgress = new JButton();
	private static JLabel lblJobName = new JLabel();
	private final JPanel panelHeaders = new JPanel();
	private final JPanel panelDetails = new JPanel();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		CommonVariables.dlgJob = new JobDialog("Test Run");
		CommonVariables.dlgJob.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		
		CommonVariables.jobStopped = false;
		
		SwingWorker<Void, Void> mySwingWorker = new SwingWorker<Void, Void>(){
			@Override
			protected Void doInBackground() throws Exception {				
				CommonVariables.dlgJob.runTestUpdate();
	            return null;
			}
		};
	    mySwingWorker.execute(); 
	    CommonVariables.dlgJob.setVisible(true);
	}
	
	public void runTestUpdate () {
		String textProgress = "TEST TEXT";
		try {
			for (int i = 0; i <= 100; i++) {
				CommonUtil.waitForCancelConfirmation();
//				if (i>10) throw new NullPointerException();
				if (!CommonVariables.jobStopped) {
					
						TimeUnit.MILLISECONDS.sleep(100);	
					
					CommonVariables.dlgJob.updateProgressBar(i,textProgress,"Testing");
				}else {
					CommonUtil.finishProgressBarStatus(false, "The job has been cancelled.", "Cancelled");
					break;
				}
			}
			CommonVariables.jobFinished = true;
		} catch (Exception e) {
			CommonUtil.catchException(JDialog.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
		}
	}

	/**
	 * Create the dialog.
	 */
	public JobDialog(String title) {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) { 
				if (progressBar.getValue()<100 && !CommonVariables.jobStopped) {
					confirmCancellation();
				} else dispose();			
			}
		});
		setTitle(title);
		setModal(true);
		setBounds(100, 100, 400, 400);
		getContentPane().setLayout(new BorderLayout());
		setLocationRelativeTo(null);
		
		getContentPane().add(panelHeaders, BorderLayout.NORTH);
		
		lblJobName.setText("Starting");
		panelHeaders.add(lblJobName);
			
		progressBar.setValue(0);
		progressBar.setStringPainted(true);
        panelHeaders.add(progressBar);
        
		btnProgress.setText("Cancel");
		btnProgress.setEnabled(true);
		panelHeaders.add(btnProgress);
        
		panelDetails.setBounds(0, 0, 384, 212);
        getContentPane().add(panelDetails);
        panelDetails.setLayout(new BorderLayout(0, 0));

        txtProgressDetails.setFont(new Font("Tahoma", Font.PLAIN, 12));      
        txtProgressDetails.setText("Waiting for job to start...");
        txtProgressDetails.setBorder(UIManager.getBorder("TextField.border"));
        txtProgressDetails.setEditable(false);
        panelDetails.add(txtProgressDetails);	
		
		btnProgress.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnProgress.getText().equals("Close")) {
					dispose();
  		        } else if (progressBar.getValue()<100 && !CommonVariables.jobStopped) {
					confirmCancellation();
  		        }	
			}
		});  
		
		progressBar.addChangeListener(new ChangeListener() {
		    public void stateChanged(ChangeEvent evt) {
			        int val = progressBar.getValue();
			        if (val >= 100) {
			        	Color colorDarkGreen = new Color(0,102,0);
			        	progressBar.setForeground(colorDarkGreen);
			        	btnProgress.setText("Close");
			        	btnProgress.setEnabled(true);
			        	java.awt.Toolkit.getDefaultToolkit().beep(); 
			          return;
			        }
		    	}
		    });
	}
	
	public void updateProgressBar(Integer value, String label, String details) {
		if (value != null) progressBar.setValue(value);
		if (label != null) lblJobName.setText(label);
		if (details != null) txtProgressDetails.setText(details);
	}
	
	public void cancelProgressBar(String details) {
		progressBar.setForeground(Color.RED);
		lblJobName.setText("Cancelled");
		if (details!=null) txtProgressDetails.setText(details);
		btnProgress.setText("Close");
		btnProgress.setEnabled(true);
	}
	
	public void cancellingRequest() {
		CommonVariables.jobCancelled = true;
		lblJobName.setText("Cancelling...");
		progressBar.setForeground(Color.RED);
		btnProgress.setText("Close");
		btnProgress.setEnabled(false);
	}
	
	public void confirmCancellation() {
		CommonVariables.confirmingCancel = true;
		int responseCancel = JOptionPane.showConfirmDialog(null, 
        		"Are you sure you want to cancel this job?", "Confirm Cancellation", JOptionPane.YES_NO_OPTION);	
    	if (responseCancel == 0) { //Cancellation confirmed.
    		CommonVariables.jobStopped = true; 
    		cancellingRequest ();
    		CommonVariables.confirmingCancel = false;
    		final WaitingDialog dlgCancelling = new WaitingDialog("Cancelling...");
    		dlgCancelling.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
    		SwingWorker<Void, Void> mySwingWorker = new SwingWorker<Void, Void>(){
				@Override
				protected Void doInBackground() throws Exception {
		            while (!CommonVariables.jobFinished) {
		            	Thread.sleep(1000);
		            }
		            return null;
				}
				@Override protected void done() {
					dlgCancelling.dispose();
					dispose();
	            }
			};
		    mySwingWorker.execute(); 
		    dlgCancelling.setVisible(true);
    	} else CommonVariables.confirmingCancel = false; //Cancellation declined.
	}
}
