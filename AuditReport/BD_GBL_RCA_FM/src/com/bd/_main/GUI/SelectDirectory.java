package com.bd._main.GUI;

import javax.swing.*;
import javax.swing.filechooser.*;

public class SelectDirectory {
	
	public static String getTargetDirectory ()
	{
		String selectedDirectory = null;
		JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		jfc.setDialogTitle("Choose a directory to save your files: ");
		jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	
		int returnValue = jfc.showOpenDialog(null);
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			if (jfc.getSelectedFile().isDirectory()) {
				selectedDirectory = jfc.getSelectedFile().toString().trim();
			}
		}
		else if (returnValue == JFileChooser.CANCEL_OPTION){
			//do nothing
		}
		else{
			System.out.println("Error on Directory SDialog Window");
		}
		return selectedDirectory;
	}
}
