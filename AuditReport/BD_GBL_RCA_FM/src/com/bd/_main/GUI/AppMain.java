package com.bd._main.GUI;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import java.util.*;

import org.jdatepicker.impl.*;

import com.bd.common.auth.*;
import com.bd.common.util.*;
import com.bd.jobs.AuditReport.Initiator_AuditReport;

public class AppMain {
	private JFrame frmReltioCustomApp;
	private Image imgBD = new ImageIcon(this.getClass().getResource("/bd_color_rgb_tag_EN_1_0.png")).getImage();
	private Image imgReltio = new ImageIcon(this.getClass().getResource("/reltio_logo.png")).getImage();
	
	private String[] timeZones = { "GMT", "EST", "CST", "MST", "PST", "IST"};
	
	private JPanel panelLogin;
	private JTextField txtUserID;
	private JPasswordField pwdUserPassword;
	private JComboBox<String> cbxEnvironment;
	private JButton btnLogIn;
	
	private JPanel panelMainMenuFM;
	private JButton btnLogOut;
	public static JButton btnAuditReport;

	private JPanel panelAuditReport;
	private JTextField txtOutputDirectoryAuditReport;
	private JTextField txtStartDateAuditReport;
	private JTextField txtEndDateAuditReport;
	private JComboBox<String> cbxTimeZone;
	private JComboBox<String> cbxObjectType;
	private JList <String> listUsersAuditReport;
	private JButton btnSelectAllAuditReport;
	private JButton btnSelectNoneAuditReport;
	private JButton btnMainMenuAuditReport;
	private JButton btnOutputDirectoryAuditReport;
	private UtilDateModel startDateModel;
	private UtilDateModel endDateModel;
	private JDatePanelImpl startDatePanel;
	private JDatePanelImpl endDatePanel;
	private JDatePickerImpl pickerStartDateAuditReport;
	private JDatePickerImpl pickerEndDateAuditReport;
	private JButton btnSubmitAuditReport;
	private JScrollPane scrollPaneUsers;

	private JLabel lblTenantDetails;
	private JLabel lblUser;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					if(LockApp.lockInstance("lockFile")) {
						AppMain window = new AppMain();
						window.frmReltioCustomApp.setVisible(true);
					}
					else {
						JOptionPane.showMessageDialog(null, "Only one instance of this application can be open at a time.", 
								"Application Error", JOptionPane.ERROR_MESSAGE);
					}
				} catch (Exception e) {
					CommonUtil.catchException(AppMain.class.getSimpleName() 
							+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AppMain() {
		CommonUtil.createLogger();
		if (CommonUtil.loadProperties()) initialize();
		else System.exit(0);	
		
//		testVariables(true, false); //TODO COMMENT OUT TEST VARIABLES METHOD
	}
	
	/**
	 * Test variables
	 */
	@SuppressWarnings("unused")
	private void testVariables(Boolean validate, Boolean skipLogin) {
		CommonVariables.testCredentials();
		
		txtUserID.setText(CommonVariables.userID); 
		pwdUserPassword.setText(CommonVariables.userPassword);
		cbxEnvironment.setSelectedIndex(0);	
		txtOutputDirectoryAuditReport.setText("C:\\Users\\10099170\\Desktop");
		
		if (validate) {
			validatingUser();
		} else {
			CommonVariables.validCredentials = true;
			btnAuditReport.setEnabled(true);
		}

//		int start = 0;
//	    int end = listUsersAuditReport.getModel().getSize() - 1;
//	    if (end >= 0) {
//	    	listUsersAuditReport.setSelectionInterval(start, end);
//	    }
		
	    if (skipLogin) {
			panelLogin.setVisible(false);
			panelMainMenuFM.setVisible(true);
	    }
	}
	
	/**
	 * Initialize the contents of the frame.
	 * @wbp.parser.entryPoint
	 */
	private void initialize() {
		frmReltioCustomApp = new JFrame();
		frmReltioCustomApp.setResizable(false);
		frmReltioCustomApp.setTitle("Reltio Custom Application");
		frmReltioCustomApp.setBounds(100, 100, 500, 525);
		frmReltioCustomApp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmReltioCustomApp.getContentPane().setLayout(new CardLayout(0, 0));
		frmReltioCustomApp.setLocationRelativeTo(null);

		createLogin();
		createMainMenuFM();
		createReltioAuditReport();
	}
	
	private void createLogin() {
		panelLogin = new JPanel();
		frmReltioCustomApp.getContentPane().add(panelLogin, "name_22311770117116");
		panelLogin.setLayout(null);
		panelLogin.setVisible(true); 
		
		JLabel lblBDLogo_Login = new JLabel("");
		lblBDLogo_Login.setIcon(new ImageIcon(imgBD));
		lblBDLogo_Login.setBounds(0, 0, 140, 90);
		panelLogin.add(lblBDLogo_Login);
		
		JLabel lblReltioLogo = new JLabel("");
		lblReltioLogo.setIcon(new ImageIcon(imgReltio));
		lblReltioLogo.setBounds(370, 0, 120, 65);
		panelLogin.add(lblReltioLogo);
	
		JLabel lblReltioCustomApp = new JLabel("Reltio Finance Application");
		lblReltioCustomApp.setHorizontalAlignment(SwingConstants.CENTER);
		lblReltioCustomApp.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblReltioCustomApp.setBounds(47, 120, 400, 40);
		panelLogin.add(lblReltioCustomApp);
		
		JLabel lblReltioUserId = new JLabel("Reltio User ID");
		lblReltioUserId.setHorizontalAlignment(SwingConstants.TRAILING);
		lblReltioUserId.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblReltioUserId.setBounds(60, 200, 85, 15);
		panelLogin.add(lblReltioUserId);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setHorizontalAlignment(SwingConstants.TRAILING);
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblPassword.setBounds(60, 240, 85, 15);
		panelLogin.add(lblPassword);
		
		JLabel lblEnviroment = new JLabel("Environment");
		lblEnviroment.setHorizontalAlignment(SwingConstants.TRAILING);
		lblEnviroment.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblEnviroment.setBounds(60, 280, 85, 15);
		panelLogin.add(lblEnviroment);
		
		txtUserID = new JTextField();
		lblReltioUserId.setLabelFor(txtUserID);
		txtUserID.setBounds(159, 200, 240, 20);
		panelLogin.add(txtUserID);
		txtUserID.setColumns(10);
		
		pwdUserPassword = new JPasswordField();
		lblPassword.setLabelFor(pwdUserPassword);
		pwdUserPassword.setBounds(159, 240, 240, 20);
		panelLogin.add(pwdUserPassword);
		
		cbxEnvironment = new JComboBox<String>();
		lblEnviroment.setLabelFor(cbxEnvironment);
		cbxEnvironment.setModel(new DefaultComboBoxModel<>(CommonVariables.propertiesEnvironmentsList));
		cbxEnvironment.setSelectedIndex(0);
		cbxEnvironment.setMaximumRowCount(3);
		cbxEnvironment.setBounds(159, 278, 240, 20);
		cbxEnvironment.setSelectedItem(null);
		panelLogin.add(cbxEnvironment);
		
		btnLogIn = new JButton("Log In");
		btnLogIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				runLogin();
			}
		});
		btnLogIn.setBounds(202, 320, 89, 23);
		panelLogin.add(btnLogIn);
		
		JLabel lblVersion = new JLabel("Version 1.0.0");
		lblVersion.setBounds(10, 472, 114, 14);
		panelLogin.add(lblVersion);
	}
	
	private void createMainMenuFM() { 
		panelMainMenuFM = new JPanel();
		frmReltioCustomApp.getContentPane().add(panelMainMenuFM, "name_2534567946830");
		panelMainMenuFM.setLayout(null);
		panelMainMenuFM.setVisible(false);
		
		JPanel panelLoginDetails = new JPanel();
		panelLoginDetails.setBounds(20, 452, 200, 34);
		panelMainMenuFM.add(panelLoginDetails);
		panelLoginDetails.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblUser = new JLabel("{User}");
		lblUser.setVerticalAlignment(SwingConstants.BOTTOM);
		panelLoginDetails.add(lblUser);
		lblUser.setHorizontalAlignment(SwingConstants.LEFT);
		
		lblTenantDetails = new JLabel("{Region - Environment}");
		lblTenantDetails.setVerticalAlignment(SwingConstants.BOTTOM);
		panelLoginDetails.add(lblTenantDetails);
		lblTenantDetails.setHorizontalAlignment(SwingConstants.LEFT);
		
		JLabel lbBDLogo_MM = new JLabel("");
		lbBDLogo_MM.setIcon(new ImageIcon(imgBD));
		lbBDLogo_MM.setBounds(0, 0, 140, 90);
		panelMainMenuFM.add(lbBDLogo_MM);
		
		JLabel lblMainMenuFM = new JLabel("Main Menu - Finance");
		lblMainMenuFM.setFont(new Font("Tahoma", Font.BOLD, 26));
		lblMainMenuFM.setHorizontalAlignment(SwingConstants.CENTER);
		lblMainMenuFM.setBounds(27, 91, 440, 50);
		panelMainMenuFM.add(lblMainMenuFM);
		
		btnLogOut = new JButton("Log Out");
		btnLogOut.setBounds(364, 11, 120, 25);
		btnLogOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CommonVariables.log.info("User logged out.");
				CommonVariables.reltioAPIService = null;
				CommonVariables.userID = null;
				CommonVariables.userPassword = null;
				CommonVariables.reltioEnvironment = null;
				CommonVariables.userTenantList = new ArrayList<String>(); ;
				CommonVariables.userRoleList = new ArrayList<String>(); 
				CommonVariables.validRoles = new ArrayList<String>(); 
				
				txtUserID.setText(null);
				pwdUserPassword.setText(null);
				cbxEnvironment.setSelectedItem(null);
				
				panelLogin.setVisible(true);
				panelMainMenuFM.setVisible(false);
				
				btnAuditReport.setEnabled(false);
				
			}
		});
		panelMainMenuFM.add(btnLogOut);
		
		btnAuditReport = new JButton("Reltio Audit Report");
		btnAuditReport.setBounds(20, 186, 200, 60);
		panelMainMenuFM.add(btnAuditReport);
		btnAuditReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelAuditReport.setVisible(true);
				panelMainMenuFM.setVisible(false);
			}
		});
		
		JLabel lblReports = new JLabel("Reports");
		lblReports.setHorizontalAlignment(SwingConstants.CENTER);
		lblReports.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblReports.setBounds(20, 152, 200, 23);
		panelMainMenuFM.add(lblReports);
		
	}
	
	@SuppressWarnings("serial")
	private void createReltioAuditReport() {
		panelAuditReport = new JPanel();
		frmReltioCustomApp.getContentPane().add(panelAuditReport, "name_366742651370");
		panelAuditReport.setLayout(null);
		panelAuditReport.setVisible(false);
		
		JLabel lblBDLogo_AR = new JLabel("");
		lblBDLogo_AR.setBounds(0, 0, 140, 90);
		lblBDLogo_AR.setIcon(new ImageIcon(imgBD));
		panelAuditReport.add(lblBDLogo_AR);
		
		JLabel lblAuditReport = new JLabel("Reltio Audit Report");
		lblAuditReport.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblAuditReport.setHorizontalAlignment(SwingConstants.CENTER);
		lblAuditReport.setBounds(55, 88, 383, 50);
		panelAuditReport.add(lblAuditReport);
		
		JLabel lblOutputDirectoryAR = new JLabel("Output Directory");
		lblOutputDirectoryAR.setLabelFor(txtOutputDirectoryAuditReport);
		lblOutputDirectoryAR.setHorizontalAlignment(SwingConstants.TRAILING);
		lblOutputDirectoryAR.setBounds(10, 145, 115, 25);
		panelAuditReport.add(lblOutputDirectoryAR);
		
		JLabel lblStartDateAR = new JLabel("Start Date");
		lblStartDateAR.setLabelFor(txtStartDateAuditReport);
		lblStartDateAR.setHorizontalAlignment(SwingConstants.TRAILING);
		lblStartDateAR.setBounds(10, 185, 115, 25);
		panelAuditReport.add(lblStartDateAR);
		
		JLabel lblEndDateAR = new JLabel("End Date");
		lblEndDateAR.setLabelFor(txtEndDateAuditReport);
		lblEndDateAR.setHorizontalAlignment(SwingConstants.TRAILING);
		lblEndDateAR.setBounds(10, 225, 115, 25);
		panelAuditReport.add(lblEndDateAR);
		
		JLabel lblTimeZone = new JLabel("TimeZone");
		lblTimeZone.setLabelFor(cbxTimeZone);
		lblTimeZone.setHorizontalAlignment(SwingConstants.TRAILING);
		lblTimeZone.setBounds(10, 265, 115, 25);
		panelAuditReport.add(lblTimeZone);
		
		JLabel lblObject = new JLabel("Object Type");
		lblObject.setLabelFor(listUsersAuditReport);
		lblObject.setHorizontalAlignment(SwingConstants.TRAILING);
		lblObject.setBounds(10, 305, 115, 15);
		panelAuditReport.add(lblObject);
		
		JLabel lblUsers = new JLabel("Changes By");
		lblUsers.setLabelFor(listUsersAuditReport);
		lblUsers.setHorizontalAlignment(SwingConstants.TRAILING);
		lblUsers.setBounds(10, 345, 115, 15);
		panelAuditReport.add(lblUsers);
				
		txtOutputDirectoryAuditReport = new JTextField();
		txtOutputDirectoryAuditReport.setEditable(false);
		txtOutputDirectoryAuditReport.setColumns(10);
		txtOutputDirectoryAuditReport.setBounds(135, 145, 297, 25);
		panelAuditReport.add(txtOutputDirectoryAuditReport);	
		
		cbxTimeZone = new JComboBox<String>();
		cbxTimeZone.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblTimeZone.setLabelFor(cbxTimeZone);
		cbxTimeZone.setModel(new DefaultComboBoxModel<>(timeZones));
		cbxTimeZone.setSelectedIndex(0);
		cbxTimeZone.setMaximumRowCount(10);
		cbxTimeZone.setBounds(135, 265, 325, 25);
		cbxTimeZone.setSelectedItem(null);
		panelAuditReport.add(cbxTimeZone);
		
		cbxObjectType = new JComboBox<String>();
		cbxObjectType.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblObject.setLabelFor(cbxObjectType);
		cbxObjectType.setModel(new DefaultComboBoxModel<>(CommonVariables.objectTypes));
		cbxObjectType.setSelectedIndex(0);
		cbxObjectType.setMaximumRowCount(10);
		cbxObjectType.setBounds(135, 305, 325, 25);
		cbxObjectType.setSelectedItem(null);
		panelAuditReport.add(cbxObjectType);
		
		scrollPaneUsers = new JScrollPane();
		scrollPaneUsers.setBounds(135, 345, 325, 100);
		panelAuditReport.add(scrollPaneUsers);
		
		listUsersAuditReport = new JList<>(new UsersModel().getListModel());
		scrollPaneUsers.setViewportView(listUsersAuditReport);
		listUsersAuditReport.setBorder(UIManager.getBorder("TextField.border"));
		listUsersAuditReport.setSelectionModel(new DefaultListSelectionModel()  {
			@Override
		    public void setSelectionInterval(int index0, int index1) 
		    {
		        if(listUsersAuditReport.isSelectedIndex(index0)) {
		        	listUsersAuditReport.removeSelectionInterval(index0, index1);
		        } else {
		        	listUsersAuditReport.addSelectionInterval(index0, index1);
		        }
		    }
		});
		lblUsers.setLabelFor(listUsersAuditReport);
		
		btnMainMenuAuditReport = new JButton("Main Menu");
		btnMainMenuAuditReport.setBounds(385, 10, 100, 25);
		btnMainMenuAuditReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtOutputDirectoryAuditReport.setText(null);
				startDateModel.setValue(null);
				endDateModel.setValue(null);
				cbxTimeZone.setSelectedItem(null);
				listUsersAuditReport.clearSelection();
				listUsersAuditReport.ensureIndexIsVisible(0);
				panelAuditReport.setVisible(false);
				panelMainMenuFM.setVisible(true);
			}
		});
		panelAuditReport.add(btnMainMenuAuditReport);
		
		btnOutputDirectoryAuditReport = new JButton("...");
		btnOutputDirectoryAuditReport.setBounds(433, 145, 27, 25);
		btnOutputDirectoryAuditReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String OutputDirectoryFile = SelectDirectory.getTargetDirectory();
				if (OutputDirectoryFile !=null) txtOutputDirectoryAuditReport.setText(OutputDirectoryFile);
			}
		});
		panelAuditReport.add(btnOutputDirectoryAuditReport);
		
		btnSelectAllAuditReport = new JButton("Select All");
		btnSelectAllAuditReport.setBounds(25, 390, 105, 25);
		btnSelectAllAuditReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int start = 0;
			    int end = listUsersAuditReport.getModel().getSize() - 1;
			    if (end >= 0) {
			    	listUsersAuditReport.setSelectionInterval(start, end);
			    }
			}
		});
		panelAuditReport.add(btnSelectAllAuditReport);
		
		btnSelectNoneAuditReport = new JButton("Select None");
		btnSelectNoneAuditReport.setBounds(25, 420, 105, 25);
		btnSelectNoneAuditReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listUsersAuditReport.clearSelection();
			}
		});
		panelAuditReport.add(btnSelectNoneAuditReport);
		
		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");
		
		startDateModel = new UtilDateModel();
		startDatePanel = new JDatePanelImpl(startDateModel, p);
		pickerStartDateAuditReport = new JDatePickerImpl(startDatePanel, new DateLabelFormatter());
		pickerStartDateAuditReport.setBounds(135, 185, 325, 30);
		panelAuditReport.add(pickerStartDateAuditReport);
		
		endDateModel = new UtilDateModel();		
		endDatePanel = new JDatePanelImpl(endDateModel, p);
		pickerEndDateAuditReport = new JDatePickerImpl(endDatePanel, new DateLabelFormatter());
		pickerEndDateAuditReport.setBounds(135, 225, 325, 30);
		panelAuditReport.add(pickerEndDateAuditReport);
		
		btnSubmitAuditReport = new JButton("Submit");
		btnSubmitAuditReport.setBounds(202, 460, 89, 23);
		btnSubmitAuditReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				runReltioAuditReport();
			}
		});
		panelAuditReport.add(btnSubmitAuditReport);			
	}
	
	private void runLogin() {
		CommonVariables.userID = null;
		CommonVariables.userPassword = null;
		CommonVariables.reltioEnvironment = null;
		CommonVariables.userTenantList = new ArrayList<String>(); ;
		CommonVariables.userRoleList = new ArrayList<String>(); 
		CommonVariables.isAdmin = false;
		
		if (txtUserID.getText().trim().length()==0) {
			JOptionPane.showMessageDialog(null, "Please enter a username.", 
					"Username Required", JOptionPane.ERROR_MESSAGE);
			txtUserID.setText(null);
			txtUserID.requestFocus();
		} else if (pwdUserPassword.getPassword().length==0) {
			JOptionPane.showMessageDialog(null, "Please enter a password.", 
					"Password Required", JOptionPane.ERROR_MESSAGE);
			pwdUserPassword.setText(null);
			pwdUserPassword.requestFocus();
		} else if (cbxEnvironment.getSelectedItem() == null) {
			JOptionPane.showMessageDialog(null, "Please select a environment.", 
					"Environment Required", JOptionPane.ERROR_MESSAGE);
			cbxEnvironment.requestFocus();
		} else {
			CommonVariables.userID = txtUserID.getText().trim();
			CommonVariables.userPassword = new String(pwdUserPassword.getPassword()).trim();
			CommonVariables.reltioEnvironment = cbxEnvironment.getSelectedItem().toString();
			
			try {
				validatingUser();
			}
			catch (Exception e){
				CommonUtil.catchException(AppMain.class.getSimpleName() 
						+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
			}
		}
	}
	
	private void runReltioAuditReport() {
		try {
			if (txtOutputDirectoryAuditReport.getText().trim().length()==0) {
				JOptionPane.showMessageDialog(null, "Please select an Output Directory.", 
						"Directory Required", JOptionPane.ERROR_MESSAGE);
				btnOutputDirectoryAuditReport.requestFocus();
			} else if (startDateModel.getValue()==null) {
				JOptionPane.showMessageDialog(null, "Please select a Start Date.",
						"Start Date Required", JOptionPane.ERROR_MESSAGE);
				pickerStartDateAuditReport.requestFocus();
			} else if (endDateModel.getValue()==null) {
				JOptionPane.showMessageDialog(null, "Please select a End Date.",
						"End Date Required", JOptionPane.ERROR_MESSAGE);
				pickerEndDateAuditReport.requestFocus();
			} else if (cbxTimeZone.getSelectedItem() == null) {
				JOptionPane.showMessageDialog(null, "Please select a Time Zone.", 
						"Time Zone Required", JOptionPane.ERROR_MESSAGE);
				cbxTimeZone.requestFocus();
			} else if (cbxObjectType.getSelectedItem() == null) {
				JOptionPane.showMessageDialog(null, "Please select an Object Type.", 
						"Object Type Required", JOptionPane.ERROR_MESSAGE);
				cbxObjectType.requestFocus();
			} else if (listUsersAuditReport.isSelectionEmpty()) {
				JOptionPane.showMessageDialog(null, "At least one Change User must be selected.", 
						"Change Users required", JOptionPane.ERROR_MESSAGE);
				listUsersAuditReport.requestFocus();
			} else {
				CommonVariables.log.info("Running 'Reltio Audit Report' job...");
				
				CommonVariables.dlgJob = new JobDialog("Publishing Reltio Audit History");
				CommonVariables.dlgJob.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
				
				String startDate = startDateModel.getYear() + "." + (startDateModel.getMonth()+1)
						+ "." + startDateModel.getDay();
				String endDate = endDateModel.getYear() + "." + (endDateModel.getMonth()+1)
						+ "." + endDateModel.getDay();
				
//				System.out.println(startDate + " " + endDate);

	          	SwingWorker<Void, Void> mySwingWorker = new SwingWorker<Void, Void>(){
					@Override
					protected Void doInBackground() throws Exception {
						synchronized (this) {	
							final Initiator_AuditReport newInitiator = 
									new Initiator_AuditReport(
											startDate,
											endDate,
											cbxTimeZone.getSelectedItem().toString(),
											cbxObjectType.getSelectedItem().toString(),
											txtOutputDirectoryAuditReport.getText().trim(),
											listUsersAuditReport.getSelectedValuesList());
							newInitiator.run();
				            return null;
						}
					}
				};
			    mySwingWorker.execute(); 
			    CommonVariables.dlgJob.setVisible(true);
			}
		}
		catch (Exception e){
			CommonUtil.catchException(AppMain.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
		}
	}
	
	private void validatingUser() {
      	final WaitingDialog dlgAuthenticating = new WaitingDialog("Authenticating...");
      	
		SwingWorker<Void, Void> mySwingWorker = new SwingWorker<Void, Void>(){
			@Override
			protected Void doInBackground() throws Exception {
				CommonVariables.validCredentials = ReltioAuthenticate.validateCredentials(CommonVariables.userID, CommonVariables.userPassword, 
						CommonVariables.reltioEnvironment, CommonVariables.authUrl, CommonVariables.tokenUrl, CommonVariables.log);
//				CommonVariables.validCredentials =true;
				if (CommonVariables.validCredentials && !CommonVariables.jobStopped) {
					if (ReltioAuthenticate.createServiceReltioAPIService(CommonVariables.tokenUrl, CommonVariables.log)) {
						UserLists.getAllUserList();
						listUsersAuditReport.setModel(new UsersModel().getListModel());;
//						System.out.println(CommonVariables.usersList);
					}
					
					for (String validRole : CommonVariables.validRoles) {	
			        	if (validRole.trim().contains("RO") || validRole.trim().contains("DS") || validRole.trim().contains("SU") 
			        			|| validRole.trim().contains("ADMIN") || validRole.trim().contains("READONLY") 
			        			|| validRole.trim().contains("SERVICE") || validRole.trim().contains("BD_DEVELOPERS") ) {
			    			btnAuditReport.setEnabled(true);
			    		}
		        	}
				
					lblUser.setText("User: " + CommonVariables.userID);
					lblTenantDetails.setText(CommonVariables.reltioEnvironment);
					
					cbxObjectType.setModel(new ObjectTypeModel().getComboModel());
					cbxObjectType.setSelectedItem(null);
					
					panelMainMenuFM.setVisible(true);
					panelLogin.setVisible(false);
				} else if(!CommonVariables.validCredentials) {
					pwdUserPassword.setText(null);
				} 
	            return null;
			}
			@Override protected void done() {
				dlgAuthenticating.dispose();
            }
		};
	    mySwingWorker.execute();
	    dlgAuthenticating.setVisible(true);
	}
}