package com.bd.jobs.AuditReport;

import java.io.BufferedWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.Map.Entry;

import com.bd.common.processor.AuditProcessor;
import com.bd.common.util.*;
import com.bd.common.writeThreadSafe.WriteThreadSafe;
import com.google.gson.*;

public class ThreadController_AuditReport implements BD_Constants, Runnable{
	private BufferedWriter auditReportBufferedWriter;
	private BufferedWriter failedBufferedWriter;	
	private Object lock = new Object();
	private boolean isSuccessfulObject = false;
	private JsonObject auditObject;
	private long currentActivityGroupNumber = 0;

	public ThreadController_AuditReport(JsonObject auditObject, long activityGrouping) {
		this.auditReportBufferedWriter = Initiator_AuditReport.auditReportBufferedWriter;
		this.failedBufferedWriter = Initiator_AuditReport.failedBufferedWriter;
		this.auditObject = auditObject;
		this.currentActivityGroupNumber = activityGrouping;
	}
	
	public void run() {
		CommonUtil.waitForCancelConfirmation();
		
		if (!CommonVariables.jobStopped) {
			synchronized (lock) {
				this.getAuditItems();
				if (isSuccessfulObject) AuditProcessor.successObjectsCount.incrementAndGet();
				else {
					WriteThreadSafe.write(failedBufferedWriter,auditObject.toString() + BD_Constants.NEW_LINE_SEPARATOR);
//					System.out.println(auditObject.toString());
					AuditProcessor.failedCount.incrementAndGet();
				}
				AuditProcessor.processedObjectsCount.incrementAndGet();
			}
		}
	}

	private void getAuditItems() {
		if (auditObject.get("itemsTotal").getAsInt() > 0) {
//			System.out.println(auditObject.toString());
			JsonArray auditItems = auditObject.get("items").getAsJsonArray();
//			System.out.println("Size:" + auditItems.size() + " - " + auditObject.get("uri"));
			
			/**
			 * Looping for every item in each audit group
			 */
			for (int j = 0; j < auditItems.size(); j++) {
				
				if (auditItems.get(j).getAsJsonObject().has("objectType")) {
					if (CommonVariables.listPropertiesAuditObjects.contains(auditItems.get(j).getAsJsonObject().get("objectType").getAsString())) {
						JsonObject itemsObject = auditItems.get(j).getAsJsonObject();
						getItemData(itemsObject);
					}
				}
			}
			isSuccessfulObject =true;
		}
		else {
			CommonVariables.log.warn("No items found:" + auditObject.toString());
		}
	}	
	
	/**
	 * Getting all the audits to write in thread safe mode
	 */
	public void getItemData(JsonObject itemsObject) {

//		System.out.println(Initiator_AuditReport.activityGrouping);
		try {
			String user = itemsObject.has("user") ? itemsObject.get("user").getAsString() : "";
			long timestamp = itemsObject.has("timestamp") ? itemsObject.get("timestamp").getAsLong() : null;

			Date date = new Date(timestamp);
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy-HH:mm:ss a");
			String[] dateArray = df.format(date).split("-");
			String dateData = dateArray[0];
			String timeData = dateArray[1];

			String url = itemsObject.has("url") ? itemsObject.get("url").getAsString() : "";
			String objectUri = itemsObject.has("objectUri") ? itemsObject.get("objectUri").getAsString() : "";

//			String dataType = itemsObject.get("data").getAsJsonObject().has("type") ? itemsObject.get("data").getAsJsonObject().get("type").getAsString() : "";
			
			if (url.contains("_update")) { // Activity for an Entity Update within the UI.
				if (url.contains(objectUri)) {  // The Activity data is related to the primary Entity that has been updated.
					buildAuditRecordData(user, dateData, timeData, itemsObject);
//					System.out.println("Contains _update with Same ObjectURI^" + dataType + "^" + user + "^" + itemsObject.toString());
				} else { //The Activity is related to a child Entity. Relationship / Reference Attribute (related to primary Entity) has been modified for that child Entity.
					if (itemsObject.get("data").getAsJsonObject().has("type")) { //TODO  When would an event not have a 'TYPE'?
						if (!itemsObject.get("data").getAsJsonObject().get("type").getAsString().contains("ENTITY_CHANGED")) { //Relationship Activity for Child Entity.
							buildAuditRecordData(user, dateData, timeData, itemsObject);
//							System.out.println("Contains _update with Different ObjectURI & Is Not Entity_Changed^" + dataType + "^" + user + "^" + itemsObject.toString());
						} else { // Entity Activity for Child Entity. The Reference Attribute URI values (related to primary Entity) being updated.
							CommonVariables.log.warn ("No activities to publish for: " + auditObject.toString());
//							System.out.println("Contains _update with Different ObjectURI & Is Entity_Changed^" + dataType + "^" + user + "^" + itemsObject.toString());
						}
					} else { //TODO  When would an event not have a 'TYPE'? I would think that this would need to fail.
						buildAuditRecordData(user, dateData, timeData, itemsObject);
//						System.out.println("Contains _update with Different ObjectURI With No Type^" + dataType + "^" + user + "^" + itemsObject.toString());
					}
				}
			} else { // Activity for a Data Load or Entity Create within the UI.
				buildAuditRecordData(user, dateData, timeData, itemsObject);
//				System.out.println("Does Not Contain _update^" + dataType + "^" + user + "^" + itemsObject.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			CommonVariables.log.error(e.getMessage() + "##" + itemsObject + "##" + currentActivityGroupNumber);
		}
	}
	
	public void buildAuditRecordData(String user, String dateData, String timeData, JsonObject itemsObject) {
		try {
			String objectValue = itemsObject.has("objectLabel") ? itemsObject.get("objectLabel").getAsString() : "";
			String objectType = itemsObject.has("objectType") ? itemsObject.get("objectType").getAsString() : "";
			String subType = "";
			if (!objectType.isEmpty()) {
				String[] objectTypeArray = objectType.split("/");
				if (objectTypeArray[objectTypeArray.length - 2].contains("relation")) {
					objectType = "Relation";
				} else if (objectTypeArray[objectTypeArray.length - 2].contains("entity")) {
					objectType = "Entity";
				}
				subType = objectTypeArray[objectTypeArray.length - 1];
			}

			String startObjectValue = itemsObject.has("startObjectLabel") ? itemsObject.get("startObjectLabel").getAsString() : "";
			String endObjectValue = itemsObject.has("endObjectLabel") ? itemsObject.get("endObjectLabel").getAsString() : "";

			String dataType = itemsObject.get("data").getAsJsonObject().has("type") ? itemsObject.get("data").getAsJsonObject().get("type").getAsString() : "";
			JsonObject deltaCollection = itemsObject.has("deltaCollection") ? itemsObject.get("deltaCollection").getAsJsonObject() : null;
			String deltaDataType = "";
			String deltaDataAttributeType = "";

			String oldValue = "";
			String newValue = "";

			if (deltaCollection != null) {
				JsonArray delta = deltaCollection.has("delta") ? deltaCollection.get("delta").getAsJsonArray() : null;
				if (delta != null) {
					for (int da = 0; da < delta.size(); da++) {
						
						oldValue="";
						newValue="";

						deltaDataType = delta.get(da).getAsJsonObject().get("type").getAsString();
						deltaDataAttributeType = delta.get(da).getAsJsonObject().has("attributeType")
								? delta.get(da).getAsJsonObject().get("attributeType").getAsString() : "";
						JsonObject deltaDataOldValue = delta.get(da).getAsJsonObject().has("oldValue")
								? delta.get(da).getAsJsonObject().get("oldValue").getAsJsonObject() : null;
						JsonObject deltaDataNewValue = delta.get(da).getAsJsonObject().has("newValue")
								? delta.get(da).getAsJsonObject().get("newValue").getAsJsonObject() : null;

						if (deltaDataOldValue != null) {
							if (deltaDataOldValue.has("value")) {
								if (deltaDataOldValue.get("value").isJsonObject()) {
									Set<Entry<String, JsonElement>> valueSet = deltaDataOldValue.get("value")
											.getAsJsonObject().entrySet();
									for (Entry<String, JsonElement> value : valueSet) {
										String key = value.getKey();
										JsonArray valueElement = value.getValue().getAsJsonArray();
										String valueElementData = valueElement.get(0).getAsJsonObject().get("value")
												.getAsString();
										oldValue += key + ":" + valueElementData + "||";
									}
								} else {
									oldValue = deltaDataOldValue.get("value").getAsString();
								}
							}
						}

						if (deltaDataNewValue != null) {
							if (deltaDataNewValue.has("value")) {
								if (deltaDataNewValue.get("value").isJsonObject()) {
									Set<Entry<String, JsonElement>> valueSet = deltaDataNewValue.get("value")
											.getAsJsonObject().entrySet();
									for (Entry<String, JsonElement> value : valueSet) {
										String key = value.getKey();
										JsonArray valueElement = value.getValue().getAsJsonArray();
										String valueElementData = valueElement.get(0).getAsJsonObject().get("value")
												.getAsString();
										newValue += key + ":" + valueElementData + "||";
									}
								} else {
									newValue = deltaDataNewValue.get("value").getAsString();
								}
							}
						}

						if (!deltaDataAttributeType.isEmpty()) {
							String[] deltaDataAttributeTypeArray = deltaDataAttributeType.split("/");
							deltaDataAttributeType = deltaDataAttributeTypeArray[deltaDataAttributeTypeArray.length
									- 1];
						}
						dataType = dataType.replace("_", " ");
						deltaDataType = deltaDataType.replace("_", " ");

						writeAuditData(dateData, timeData, user, objectType,
								subType, objectValue, startObjectValue, endObjectValue, dataType, deltaDataType,
								deltaDataAttributeType, oldValue, newValue, CommonVariables.delimiter);
					}
				} else {
					dataType = dataType.replace("_", " ");
					deltaDataType = deltaDataType.replace("_", " ");

					writeAuditData(dateData, timeData, user, objectType,
							subType, objectValue, startObjectValue, endObjectValue, dataType, deltaDataType,
							deltaDataAttributeType, oldValue, newValue, CommonVariables.delimiter);
				}
			} else {
				dataType = dataType.replace("_", " ");
				deltaDataType = deltaDataType.replace("_", " ");

				writeAuditData(dateData, timeData, user, objectType,
						subType, objectValue, startObjectValue, endObjectValue, dataType, deltaDataType,
						deltaDataAttributeType, oldValue, newValue, CommonVariables.delimiter);
			}
//			isSuccessfulObject =true;
			
		} catch (Exception e) {
			e.printStackTrace();
			CommonVariables.log.error(e.getMessage() + "##" + itemsObject + "##" + currentActivityGroupNumber);
		}
	}
	
	private synchronized Boolean writeAuditData(String dateData, String timeData, String user,
			String objectType, String subType, String objectValue, String startObjectValue,
			String endObjectValue, String dataType, String deltaDataType, String deltaDataAttributeType,
			String oldValue, String newValue, String delimiter) {			
		try {
			StringBuffer sb = new StringBuffer();
			
			if (dateData != "") {sb.append(dateData);} else sb.append("-");
			sb.append(delimiter);
			if (timeData != "") {sb.append(timeData);} else sb.append("-");
			sb.append(delimiter);
			if (user != "") {sb.append(user);} else sb.append("-");
			sb.append(delimiter);
			sb.append(currentActivityGroupNumber);
			sb.append(delimiter);
			if (objectType != "") {sb.append(objectType);} else sb.append("-");
			sb.append(delimiter);
			if (subType != "") {sb.append(subType);} else sb.append("-");
			sb.append(delimiter);
			if (objectValue != "") {sb.append(objectValue);} else sb.append("-");
			sb.append(delimiter);
			if (startObjectValue != "") {sb.append(startObjectValue);} else sb.append("-");
			sb.append(delimiter);
			if (endObjectValue != "") {sb.append(endObjectValue);} else sb.append("-");
			sb.append(delimiter);
			if (dataType != "") {sb.append(dataType);} else sb.append("-");
			sb.append(delimiter);
			if (deltaDataType != "") {sb.append(deltaDataType);} else sb.append("-");
			sb.append(delimiter);
			if (deltaDataAttributeType != "") {sb.append(deltaDataAttributeType);} else sb.append("-");
			sb.append(delimiter);
			if (oldValue != "") {sb.append(oldValue);} else sb.append("-");
			sb.append(delimiter);
			if (newValue != "") {sb.append(newValue);} else sb.append("-");
			sb.append(delimiter);
			
			sb.append(BD_Constants.NEW_LINE_SEPARATOR);

			if(WriteThreadSafe.write(auditReportBufferedWriter,sb.toString())) {
				AuditProcessor.successRecordsCount.incrementAndGet();			
				return true;	
			}
		} catch (Exception e) {
			CommonUtil.recordThreadException (e, failedBufferedWriter, "Group#: " + currentActivityGroupNumber);
		}		
		return false;
	}	
}
