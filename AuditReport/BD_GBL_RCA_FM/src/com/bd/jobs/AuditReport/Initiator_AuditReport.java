package com.bd.jobs.AuditReport;

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;

import javax.swing.*;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import com.bd._main.GUI.*;
import com.bd.common.auth.ReltioAuthenticate;
import com.bd.common.processor.AuditProcessor;
import com.bd.common.util.*;

public class Initiator_AuditReport {	
	private static String jobName = "Reltio Audit Report";
	private static String directoryName = "ReltioAuditReport";	
	
	private static long startTime;
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");	
	private String queryFilter;
	private String startDate;
	private String endDate;
	private String timeZone;
	private String objectType;
	private String targetDirectory;
	private List<String> listSelectedUsers;
	
	public static List<String> selectedUsers;
	public static BufferedWriter auditReportBufferedWriter;
	public static BufferedWriter failedBufferedWriter;
	public static String auditReportFileName;
	public static String failedFileName;
	
	public Initiator_AuditReport(String startDate, String endDate, String timeZone, String objectType, String targetDirectory, List<String> listSelectedUsers) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.timeZone = timeZone;
		this.objectType = objectType;
		this.targetDirectory = targetDirectory;
		this.listSelectedUsers = listSelectedUsers;
		selectedUsers = this.listSelectedUsers;
	}

	public static void main(String[] args) throws Exception {
		testRun();
	}
	
	private static void testRun() {
		CommonUtil.createLogger();
		if (CommonUtil.loadProperties()) {
			CommonVariables.log.info("Running " + jobName + " job...");
			System.out.println("Running " + jobName + " job...");
			
			CommonVariables.testCredentials();
			
			CommonVariables.validCredentials = ReltioAuthenticate.validateCredentials(CommonVariables.userID, CommonVariables.userPassword, 
					CommonVariables.reltioEnvironment, CommonVariables.authUrl, CommonVariables.tokenUrl, CommonVariables.log);
			
			if (!CommonVariables.validCredentials) System.exit(0);
			
			String testTimeZone;
			String testTargetDirectory;
			String testStartDate;
			String testEndDate;
			String testObjectType;
			List<String> testSelectedUsers;
			
			testStartDate="2019.06.01";
			testEndDate="2019.07.04";
			testTimeZone = "EST";
			testObjectType = "Chart Of Accounts";
			testTargetDirectory = "P:\\Users\\10282741\\Desktop";
			testSelectedUsers = new ArrayList<String>();
			testSelectedUsers.add("rosalinda.patlan");
			testSelectedUsers.add("sara.steinle");
			testSelectedUsers.add("svc_mule");
	
			CommonVariables.dlgJob = new JobDialog("Creating Audit Report");
			CommonVariables.dlgJob.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	
	      	SwingWorker<Void, Void> mySwingWorker = new SwingWorker<Void, Void>(){
				@Override
				protected Void doInBackground() throws Exception {
					final Initiator_AuditReport newInitiator = 
							new Initiator_AuditReport (testStartDate,testEndDate,testTimeZone,testObjectType,testTargetDirectory,testSelectedUsers);
					newInitiator.run();
		            return null;
				}
			};
		    mySwingWorker.execute(); 
		    CommonVariables.dlgJob.setVisible(true);
		}
		else System.exit(0);
	}
		
	public void run() {
		CommonVariables.jobStopped = false;
		CommonVariables.jobCancelled = false;
		CommonVariables.jobFinished = false;
		CommonVariables.caughtException = false;
		CommonVariables.caughtError = false;
		CommonVariables.confirmingCancel = false;
		
		CommonVariables.log.info("Starting " + jobName + " Job.");
		CommonVariables.log.info("Job Start Time: "+ dateFormat.format(new Date()));
		startTime = System.nanoTime();
		
		if (CommonVariables.validCredentials) {	
			createBufferedWriters();
			
			long filterStartTime = 0;
			long filterEndTime = 0;
			
			DateFormat formatter = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z");
			try {
				filterStartTime = formatter.parse(startDate + " 00:00:00 " + timeZone).getTime();
				filterEndTime = formatter.parse(endDate + " 23:59:59 " + timeZone).getTime();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String objectTypesFilter="";
			for (String obj: CommonVariables.propertiesAuditObjectsMap.get(objectType)) {
				objectTypesFilter += "equals(items.objectType,'" + obj + "') or ";
				CommonVariables.listPropertiesAuditObjects.add(obj);
			}
			objectTypesFilter = objectTypesFilter.trim();
			objectTypesFilter = objectTypesFilter.substring(0, objectTypesFilter.length() - 2);
			
			String selectedUsersFilter ="";
			for (String user : listSelectedUsers) {
				selectedUsersFilter += "equals(user,'" + user + "') or ";
			}
			selectedUsersFilter = selectedUsersFilter.trim();
			selectedUsersFilter = selectedUsersFilter.substring(0, selectedUsersFilter.length() - 2);

			queryFilter = "filter=range(timestamp," + filterStartTime + "," + filterEndTime + ") "
					+ "and (" + objectTypesFilter.trim() + ") and (" + selectedUsersFilter.trim() + ")";
			queryFilter = queryFilter.trim();
			
			//System.out.println(queryFilter);
			
			final AuditProcessor newProcessor = new AuditProcessor (queryFilter, 1);
			newProcessor.runProcess();
			
			CommonUtil.waitForCancelConfirmation();
			closeBufferedWriters();
			filesCleanup(targetDirectory);
			
			if (!CommonVariables.jobStopped) {
				publishLogResults();
				CommonUtil.finishProgressBarStatus(true, "Job Complete.", progressBarDetails("Job Complete."));
			}
			else if (CommonVariables.jobCancelled) CommonUtil.finishProgressBarStatus(false, "The job has been cancelled.", null);
			else CommonUtil.finishProgressBarStatus(false, null, null);

		} else {
			CommonVariables.log.warn("The user credentials could not be validated."); 
			JOptionPane.showMessageDialog(null, "The user credentials could not be validated.", 
					"Access Denied", JOptionPane.ERROR_MESSAGE); 
			CommonVariables.dlgJob.dispose();		
		}			
		CommonVariables.log.info("Ending " + jobName + " Job."); 
		CommonVariables.log.info("Job End Time: "+ dateFormat.format(new Date()));
		CommonVariables.jobFinished = true;
	}
	
	private void createBufferedWriters() {
		CommonUtil.updateProgressBarStatus(0, "Running", "Creating Output Files...", null, false);
		Calendar calendar = Calendar.getInstance();
		String timestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(calendar.getTime());
		
		targetDirectory = targetDirectory + "\\" + directoryName + "_"  + objectType + "_" + timestamp;
		auditReportFileName = targetDirectory + "\\" + CommonVariables.auditReportFileName + "_" + objectType + "_" + timestamp + ".csv";
		failedFileName = targetDirectory + "\\" + CommonVariables.failedFileName + "_" + timestamp + ".txt";
		
		if (!CommonUtil.createOutputDirectory(targetDirectory)) {
			CommonUtil.updateProgressBarStatus(null, "Stopping", "Failed to create output directory.", null, false);
			return;
		}
		String fileHeaders = getFileHeaders(CommonVariables.delimiter);
		auditReportBufferedWriter = CommonUtil.createBufferedFile(auditReportFileName, fileHeaders);
		if(auditReportBufferedWriter==null) {
			CommonUtil.updateProgressBarStatus(null, "Stopping", "Failed to create '" + auditReportFileName + "' file.", null, false);
			return;	
		}
		failedBufferedWriter =  CommonUtil.createBufferedFile(failedFileName, null);
		if(failedBufferedWriter == null){ 
			CommonUtil.updateProgressBarStatus(null, "Stopping", "Failed to create '" + failedFileName + "' file.", null, false);
			return;
		}
	}
	
	private void closeBufferedWriters() {
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
	
		CommonUtil.closeBufferedWriter(failedBufferedWriter, methodName);
		CommonUtil.closeBufferedWriter(auditReportBufferedWriter, methodName);
	}
	
	private static void filesCleanup(String targetDirectory) {
		try {		
			if (CommonVariables.jobStopped) {
				CommonUtil.deleteFile(failedFileName);
				CommonUtil.deleteFile(auditReportFileName);
				CommonUtil.deleteDirectory(targetDirectory);
				
			} else {
				CommonUtil.updateProgressBarStatus(95, "Finishing", "Saving output files...", progressBarDetails("Saving output files..."), false);
				if (AuditProcessor.failedCount.longValue() == 0) {
					CommonUtil.deleteFile(failedFileName);
				}	
				if (AuditProcessor.successRecordsCount.longValue() > 0 && AuditProcessor.successRecordsCount.longValue() <= 100000){
					
					String outputFileName = auditReportFileName.replace(".csv", ".xlsx");
					CommonVariables.log.info("Excel (" + outputFileName + ") generation started");
					XSSFWorkbook excelWkb = new XSSFWorkbook();
					XSSFSheet excelSheet = excelWkb.createSheet("PotentialMatches");
					
					CommonUtil.csvRead(auditReportFileName,CommonVariables.delimiter, excelWkb, excelSheet);
					
					CommonUtil.addExcelHeaderCellStyle(excelWkb, excelSheet, CommonVariables.colorGrey, IndexedColors.WHITE.getIndex(), true, 0, 0, 3);
					CommonUtil.addExcelHeaderCellStyle(excelWkb, excelSheet, CommonVariables.colorGreen, IndexedColors.WHITE.getIndex(), false, 0, 4, 10); 
					CommonUtil.addExcelHeaderCellStyle(excelWkb, excelSheet, CommonVariables.colorBlue, IndexedColors.WHITE.getIndex(), false, 0, 11, 13); 

					int totalExcelCols = excelSheet.getRow(0).getPhysicalNumberOfCells();
					CommonUtil.addExcelHeaderFilter(excelSheet, 0, 0, totalExcelCols - 1);
					
					int recordCount = (int)AuditProcessor.successRecordsCount.longValue();
					CommonUtil.addExcelDataValidation(excelSheet, new String[]{"Merge", "Not a Match"}, 1, recordCount - 1, 0, 0);				
					
					CommonUtil.finishExcelWorkBook(excelWkb, outputFileName);
					CommonVariables.log.info("Excel (" + outputFileName + ") has been generated");
					
					CommonUtil.deleteFile(auditReportFileName);
					
				} else if (AuditProcessor.successRecordsCount.longValue() > 100000) {
					JOptionPane.showMessageDialog(null, "Records count exceeds the max limit of an Excel file. A '.csv' file has been generated.", 
							"Record Count Warning", JOptionPane.ERROR_MESSAGE);
				} else CommonUtil.deleteFile(auditReportFileName);
			}
		} catch (Exception e) {
			CommonUtil.catchException(CommonUtil.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
		}
	}

	private static String getFileHeaders(String delimiter) {
		String fileHeaders = 
				"Date Completed" + delimiter +	
				"Time" + delimiter +	
				"Changed By" + delimiter +
				"Activity Grouping" + delimiter +
				"Entity / Relation" + delimiter +	
				"Entity Name / Relation Name" + delimiter +					
				"Entity ID" + delimiter +
				"Relationship From Entity ID" + delimiter +
				"Relationship To Entity ID" + delimiter +
				"Activity Type" + delimiter +
				"Activity Sub-Type" + delimiter +
				"Attributes" + delimiter +
				"Attribute Old Value" + delimiter +
				"Attribute New Value" 
				;	
		return fileHeaders;
	}
	
	private static void publishLogResults() {
		CommonVariables.log.info("Execution Completed...");
		
		CommonVariables.log.info("Audit Objects Scanned: " + AuditProcessor.scannedObjects);

		CommonVariables.log.info("Objects Processed Successfully: " + AuditProcessor.successObjectsCount);
		
		CommonVariables.log.info("Objects Failed to Process: " + AuditProcessor.failedCount);
		
		CommonVariables.log.info("Records Published: " + AuditProcessor.successRecordsCount);
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		CommonVariables.log.info("End Time: "+ dateFormat.format(date));
		
		long duration = (System.nanoTime() - startTime); 		
		
		CommonVariables.log.info("Total execution time: " +
                String.format("%d hrs, %d min, %d sec",
                        TimeUnit.NANOSECONDS.toHours(duration),
                        TimeUnit.NANOSECONDS.toMinutes(duration) -
                        	TimeUnit.HOURS.toMinutes(TimeUnit.NANOSECONDS.toHours(duration)),
                        TimeUnit.NANOSECONDS.toSeconds(duration) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.NANOSECONDS.toMinutes(duration))));
	}
	
	public static String progressBarDetails(String currentActivity) {
		long duration = (System.nanoTime() - startTime); 
		String textProgress = 
        		"Audit Objects to Process: " + AuditProcessor.scannedObjects
        		+ "\n"
        		+ "\nProcessed: " + AuditProcessor.processedObjectsCount 
        		+ "\nSuccess: " + AuditProcessor.successObjectsCount 
        		+ "\nFailed: " + AuditProcessor.failedCount
        		+ "\n"
        		+ "\nAudit Records Published: " + AuditProcessor.successRecordsCount 
        		+ "\n"
        		+ "\nCurrent Activity: " + currentActivity 
        		+ "\n"
        		+ "\nCurrent Run Time: " +
                String.format("%d hrs, %d min, %d sec",
                        TimeUnit.NANOSECONDS.toHours(duration),
                        TimeUnit.NANOSECONDS.toMinutes(duration) -
                        	TimeUnit.HOURS.toMinutes(TimeUnit.NANOSECONDS.toHours(duration)),
                        TimeUnit.NANOSECONDS.toSeconds(duration) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.NANOSECONDS.toMinutes(duration)));
        		;
		return textProgress;
	}
	
	public static Integer progressBarPercent() {
		double calculatePercentComplete =  (double) ((double) AuditProcessor.processedObjectsCount.longValue() / (double) AuditProcessor.scannedObjects.longValue() * 90);
		int percentComplete = (int) calculatePercentComplete + 5;
//		System.out.println("A:" + (int) percentComplete + " - T:" + totalLineCount + " - C:" + currentCount);
		return percentComplete;
	}
}

