package com.bd.common.logger;

import org.apache.log4j.FileAppender;

public class LoggerDateAppender extends FileAppender {
	public LoggerDateAppender() {
    }

    @Override
    public void setFile(String file) {
        super.setFile(prependDate(file));
    }

    private static String prependDate(String filename) {
        return "BD_Reltio_Custom_App"  + filename;
//        return "EXTERNAL_MATCH_MERGE_" + System.currentTimeMillis() + filename;
    }
}
