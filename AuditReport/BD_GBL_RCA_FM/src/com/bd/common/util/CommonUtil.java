package com.bd.common.util;

import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.*;
import org.apache.poi.xssf.usermodel.*;
import org.json.*;

import com.bd._main.GUI.CatchException;
import com.bd.common.auth.ReltioAuthenticate;
import com.bd.common.bo.Attribute;
import com.google.gson.*;

public class CommonUtil {
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	private static Date eDate = new Date();
	private static Date date = new Date();
    
    public static String apiGetRequest(String apiCall, String parentMethodName) {
    	String apiResponse = null;
		try {
			apiResponse = CommonVariables.reltioAPIService.get(apiCall);
			return apiResponse;
		} catch (Exception e) {
			CommonUtil.catchException(ReltioAuthenticate.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName()
					+ " - " + parentMethodName + ".", e, CommonVariables.log);
		}
    	return null;
	}	
	
	public static String apiPostRequest(String apiCall, String apiBody, String parentMethodName) {
		
		String apiResponse = null;
		try {
			apiResponse = CommonVariables.reltioAPIService.post(apiCall, apiBody);
			return apiResponse;
		} catch (Exception e) {
			CommonUtil.catchException(ReltioAuthenticate.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName()
					+ " - " + parentMethodName + ".", e, CommonVariables.log);
		}
    	return null;
	}

	public static Boolean checkNull(String value) {
		if (value != null && !value.trim().equals("")
				&& !value.trim().equals("UNKNOWN")
				&& !value.trim().equals("<blank>")
				&& !value.trim().equalsIgnoreCase("Unspecified")
				&& !value.trim().equals("N/A")) {
			return true;
		}
		return false;
	}
	
	public static String findTrueOVValue(List<Attribute> list) {
		String paramValue = null;
		if (checkList(list)) {
			for (Iterator<Attribute> iterator = list.iterator(); iterator.hasNext();) {
				Attribute attribute = (Attribute) iterator.next();
				if (attribute.isOv()) {
					paramValue = attribute.getValue();
					break;
				}
			}
		}
		return paramValue;
	}
	
	public static String findTrueOVLookupCode(List<Attribute> list) {
		String paramValue = null;
		if (checkList(list)) {
			for (Iterator<Attribute> iterator = list.iterator(); iterator.hasNext();) {
				Attribute attribute = (Attribute) iterator.next();
				if (attribute.isOv()) {
					paramValue = attribute.getLookupCode();
					break;
				}
			}
		}
		return paramValue;
	}
	
	public static String findTrueOVLookupValue(List<Attribute> attribute) {
		String paramValue = null;
		if (checkList(attribute)) {
			for (Iterator<Attribute> iterator = attribute.iterator(); iterator.hasNext();) {
				Attribute attr = (Attribute) iterator.next();
				if (attr.isOv()) {
					paramValue = attr.lookupCode;
					break;
				}
			}
		}
		return paramValue;
	}
	
	public static String findValue(List<Attribute> list) {
		String paramValue = null;
		if (checkList(list)) {
			for (Iterator<Attribute> iterator = list.iterator(); iterator.hasNext();) {
				Attribute attribute = (Attribute) iterator.next();
				if (attribute.getValue()!=null) {
					paramValue = attribute.getValue();
					break;
				}
			}
		}
		return paramValue;
	}
	
	public static String findTrueOVValueUnique(List<Attribute> list) {
		String paramValue = null;
		String paramValue1 = null;
		Boolean check=false;
		if (checkList(list)) {
			for (Iterator<Attribute> iterator = list.iterator(); iterator.hasNext();) {
				Attribute attribute = (Attribute) iterator.next();
				if (attribute.isOv()) {
					paramValue1 = attribute.getValue();
				}
			}
			for (Iterator<Attribute> iterator = list.iterator(); iterator.hasNext();) {
				Attribute attribute = (Attribute) iterator.next();
				if(attribute.getValue()!=paramValue1){
					check=true;
					break;
				}
			}
			if(!check){
				paramValue=paramValue1;
			}
		}
		return paramValue;
	}
	
	public static String findTrueOVUriUnique(List<Attribute> list) {
		String paramValue = null;
		if (checkList(list)) {
			for (Iterator<Attribute> iterator = list.iterator(); iterator.hasNext();) {
				Attribute attribute = (Attribute) iterator.next();
				if (attribute.isOv() && list.size()==1) {
					paramValue = attribute.getUri();
					break;
				}
			}
		}
		return paramValue;
	}
	
	public static String findTrueOVUri(List<Attribute> list) {
		String paramValue = null;
		if (checkList(list)) {
			for (Iterator<Attribute> iterator = list.iterator(); iterator.hasNext();) {
				Attribute attribute = (Attribute) iterator.next();
				if (attribute.isOv()) {
					paramValue = attribute.getUri();
					break;
				}
			}
		}
		return paramValue;
	}

	public static Boolean checkList(List<Attribute> collection) {
		if (collection != null && !collection.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

	public static Boolean checkIfNull(Object toCheckValue) {
		if (toCheckValue != null && !toCheckValue.toString().trim().isEmpty()) {
			return true;
		}
		return false;
	}
	
	public static Object getKeyFromValue(HashMap<String,String> hashMap, Object value) {
	    for (Object o : hashMap.keySet()) {
	      if (hashMap.get(o).equals(value)) {
	        return o;
	      }
	    }
	    return null;
	}
	
	public static JSONArray createAttributeArray (String attributeValue, String entityTypes, String attributeName) {
		JSONArray arrayAttribute = new JSONArray();
		JSONObject objItem = new JSONObject();
		
		if (entityTypes!=null) objItem.put("type", "configuration/entityTypes/" + entityTypes + "/attributes/" + attributeName);
		objItem.put("value", attributeValue);
		arrayAttribute.put(objItem);
		
		return arrayAttribute;
	}
	
	public static JSONArray createNestedAttributeArray (String attributeValue, String entityTypes, String parentAttributeName, String attributeName) {
		JSONArray arrayNestedAttributes = new JSONArray();
		JSONObject objNestedAttributeValue = new JSONObject();
		JSONObject objNestedAttribute = new JSONObject();
		JSONArray arrayItems = new JSONArray();
		JSONObject objItem = new JSONObject();
		
		if (entityTypes!=null) objItem.put("type", "configuration/entityTypes/" + entityTypes + "/attributes/" + parentAttributeName + "/attributes/" + attributeName);
		objItem.put("value", attributeValue);
		arrayItems.put(objItem);
		if (entityTypes!=null) objNestedAttribute.put(parentAttributeName, arrayItems);
		else objNestedAttribute.put(attributeName, arrayItems);
		objNestedAttributeValue.put("value", objNestedAttribute);
		arrayNestedAttributes.put(objNestedAttributeValue);
		
		return arrayNestedAttributes;
	}
	
	public static JSONObject createCrosswalkObject (String crosswalkValue,String source) {
		JSONObject objCrosswalk = new JSONObject();
		JSONArray arrayItems = new JSONArray();
		JSONObject objItem = new JSONObject();
		
		objItem.put("type", "configuration/sources/" + source);
		objItem.put("value", crosswalkValue);
		arrayItems.put(objItem);
		objCrosswalk.put("crosswalks", arrayItems);
		
		return objCrosswalk;
	}
	
	/**
	 * Load main properties variables
	 */
	public static Boolean loadProperties() {
		Boolean loadCompleted = false;
		Properties prop = new Properties();
		InputStream input = null;
		
		try
		{
			input = new FileInputStream(CommonVariables.mainPropertiesFile);
			prop.load(input);
			CommonVariables.authUrl = prop.getProperty("AUTH_URL");
			CommonVariables.tokenUrl = CommonVariables.authUrl + "/token";
			CommonVariables.propertiesEnvironments = prop.getProperty("ENVIRONMENTS");
			CommonVariables.propertiesEnvironmentsList = CommonVariables.propertiesEnvironments.trim().split(","); 
			CommonVariables.auditReportFileName = prop.getProperty("AUDIT_FILE_NAME");
			CommonVariables.failedFileName = prop.getProperty("FAILED_FILE_NAME");
			CommonVariables.threadCount = Integer.parseInt(prop.getProperty("THREAD_COUNT"));
			CommonVariables.scanLimit = Integer.parseInt(prop.getProperty("SCAN_LIMIT"));
			CommonVariables.delimiter=prop.getProperty("DELIMITER");	
            loadCompleted = true;
		}
		catch (Exception e) {
			if (e instanceof FileNotFoundException) {
				CommonUtil.showErrorMessage ("Application Error", "Properties file '" + CommonVariables.mainPropertiesFile + "' not found.", CommonVariables.log);
			} else CommonUtil.catchException(CommonVariables.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
		} finally {
		    if (input != null) {
		        try {
		            input.close();
		        } catch (Exception e) {
		        	CommonUtil.catchException(CommonVariables.class.getSimpleName() 
							+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
		        }
		    }
		}
		return loadCompleted;
	}
	
	/**
	 * Create logger.
	 */
	public static void createLogger() {
		String currentDirectory = System.getProperty("user.dir");
		String loggerDirectory = currentDirectory + "\\logs";
		Path path = Paths.get(loggerDirectory);
//		//Create Logger & Append Initial Details
		if (Files.notExists(path)) {
			new File(loggerDirectory).mkdirs();
		}
		
		System.setProperty("logfile.name", loggerDirectory + "\\BD_App_FinanceMaster" +".log");
		CommonVariables.log = Logger.getLogger(CommonUtil.class.getSimpleName());
		CommonVariables.log.info("Start Time: "+ dateFormat.format(date));
	}
	
	/**
	 * This method converts given timezone into desired timezone
	 */

	public static String convertTimeZone(String inputDate, String outputTimezone) {
		SimpleDateFormat oldFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		oldFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		SimpleDateFormat newFormat = new SimpleDateFormat("MM/dd/yyyy,HH:mm:ss z");
		String newDate = null;
		int timezoneCode = getTimeZoneCode(outputTimezone);
	
		try {
			switch (timezoneCode) {
				case 1: {
					Date oldDate = oldFormat.parse(inputDate);
					newFormat.setTimeZone(TimeZone.getTimeZone("US/Eastern"));
					newDate = newFormat.format(oldDate);
					break;
				}
				case 2: {
					Date oldDate = oldFormat.parse(inputDate);
					newFormat.setTimeZone(TimeZone.getTimeZone("IST"));
					newDate = newFormat.format(oldDate);
					break;
				}
				case 3: {
					Date oldDate = oldFormat.parse(inputDate);
					newFormat.setTimeZone(TimeZone.getTimeZone("PST"));
					newDate = newFormat.format(oldDate);
					break;
				}
				case 4: {
					Date oldDate = oldFormat.parse(inputDate);
					newFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
					newDate = newFormat.format(oldDate);
					break;
				}
				case 5: {
		            Date oldDate = oldFormat.parse(inputDate);
					newFormat.setTimeZone(TimeZone.getTimeZone("CST"));
					newDate = newFormat.format(oldDate);
					break;
				}
				default: {
					Date oldDate = oldFormat.parse(inputDate);
					newDate = newFormat.format(oldDate);
				}
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return newDate;
	}
	
	private static int getTimeZoneCode(String outputTimezone) {
		if (outputTimezone.equalsIgnoreCase("EST")) return 1;
		else if (outputTimezone.equalsIgnoreCase("IST")) return 2;
		else if (outputTimezone.equalsIgnoreCase("PST"))  return 3;
		else if (outputTimezone.equalsIgnoreCase("GMT"))  return 4;
		else if(outputTimezone.equalsIgnoreCase("CST")) return 5;
		else return 0;
	}
	
	/**
	 * Create Output Directory.
	 */
	public static Boolean createOutputDirectory(String outputDirectory) {
		return new File(outputDirectory).mkdirs();
	}
	
	
	/**
	 * This method creates a File Input Stream.
	 */
	public static FileInputStream createFileInputStream(String fileName) {
		FileInputStream fileInputStream = null;
		try {
			fileInputStream = new FileInputStream(new File(fileName));
			return fileInputStream;
		} catch (Exception e) {
			CommonUtil.catchException(CommonUtil.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
		}
		return null;
	}
	
	/**
	 * This method creates a Buffered Write file.
	 */
	public static BufferedWriter createBufferedFile(String fileName, String headers) {
		BufferedWriter bufferedWriter = null;
		
		try {
			bufferedWriter = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(fileName, true)));
			if (headers !=null) {
				bufferedWriter.append(headers + BD_Constants.NEW_LINE_SEPARATOR);
				bufferedWriter.flush();
			}
			return bufferedWriter;
		} catch (Exception e) {
			CommonUtil.catchException(CommonUtil.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
		}
		return null;
	}
	
	/**
	 * This method creates opens a buffered reader.
	 */
	public static BufferedReader createBufferedReader (FileInputStream fileInputStream) {
		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new InputStreamReader(
					fileInputStream, "UTF8"));
			return bufferedReader;
		} catch (Exception e) {
			CommonUtil.catchException(CommonUtil.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
		}
		return null;
	}
	
	/**
	 * This method creates closes a buffered reader.
	 */
	public static void closeBufferedReader (BufferedReader bufferedReader) {
		try {
			bufferedReader.close();
		} catch (Exception e) {
			CommonUtil.catchException(CommonUtil.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
		}
	}

	/**
	 * This method creates a File write file.
	 */
	public static FileWriter createFile(String fileName, String headers) {
		FileWriter fileWriter = null;
		try {
			File file = new File(fileName);
			fileWriter = new FileWriter(file, true);
			fileWriter.flush();	
			if (headers !=null) {
				fileWriter.append(headers + BD_Constants.NEW_LINE_SEPARATOR);
				fileWriter.flush();
			}
			return fileWriter;
		} catch (Exception e) {
			CommonUtil.catchException(CommonUtil.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
		}
		return null;
	}
	
	/**
	 * This method close a BufferedWriter.
	 */
	public static void closeBufferedWriter(BufferedWriter bufferedWriter, String parentMethodName) {
		try {
			bufferedWriter.close();
		}
		catch (Exception e){
			CommonUtil.catchException(CommonUtil.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName()
					+ " - " + parentMethodName + ".", e, CommonVariables.log);
		}
	}
	
	/**
	 * This method close a FileWriter.
	 */
	public static void closeFileWriter(FileWriter fileWriter, String parentMethodName) {
		try {
			fileWriter.close();
		}
		catch (Exception e){
			CommonUtil.catchException(CommonUtil.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName()
					+ " - " + parentMethodName + ".", e, CommonVariables.log);
		}
	}
	
	/**
	 * This method close a FileInputStream.
	 */
	public static void closeFileInputStream(FileInputStream fileInputStream, String parentMethodName) {
		try {
			fileInputStream.close();
		}
		catch (Exception e){
			CommonUtil.catchException(CommonUtil.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName()
					+ " - " + parentMethodName + ".", e, CommonVariables.log);
		}
	}

	/**
	 * This method decodes a string.
	 */
	public static String decodeString(String value, String parentMethodName) {
		try {
			value = URLDecoder.decode(value, "UTF-8");
			return value;
		}
		catch (Exception e){
			CommonUtil.catchException(CommonUtil.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName()
					+ " - " + parentMethodName + ".", e, CommonVariables.log);
			return null;
		}
	}
	
	/**
	 * This method retrieves the cursor of the existing API response.
	 */
	public static String retrieveCursor(JsonObject objResponse) {
		/**Record the cursor from the current response*/
		try {
			JsonObject objResponseCursor = objResponse.get("cursor").getAsJsonObject();
			String cursorValue = objResponseCursor.get("value").getAsString();
			
			JSONObject objCursorValue = new JSONObject();
			JSONObject objCursor = new JSONObject();
			objCursorValue.put("value", cursorValue);
			objCursor.put("cursor", objCursorValue);
			return objCursor.toString();
		}
		catch (Exception e){
			CommonUtil.catchException(CommonUtil.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
			return null;
		}	
	}
	
	/**
	 * This method creates opens an Excel Workbook.
	 */
	public static XSSFWorkbook openExcelWorkbook (FileInputStream fileInputStream) {
		try {
			XSSFWorkbook excelWkb = new XSSFWorkbook(fileInputStream);
			return excelWkb;
		} catch (Exception e) {
			CommonUtil.catchException(CommonUtil.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
		}
		return null;
	}	
	
	/**
	 * This method creates opens an Excel Workbook.
	 */
	public static void closeExcelWorkbook (Workbook excelWkb) {
		try {
			excelWkb.close();
		} catch (Exception e) {
			CommonUtil.catchException(CommonUtil.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
		}
	}	
	
	public static Boolean validInputSheets(Workbook wb)
	{
		int totalSheets = 0;
		totalSheets = wb.getNumberOfSheets();
		    
		if (totalSheets==1) return true;
	    else {
	    	JOptionPane.showMessageDialog(null, "The Input Excel workbook file can only have ONE worksheet tab.", 
					"Incorrect Workbook", JOptionPane.ERROR_MESSAGE);
	    	CommonVariables.log.error("Invalid number of worksheets in input file. Only one worksheet can exist.");
	    	return false;
	    } 
	}
	
	public static String getExcelRow (Row row, Integer intCol) {
		Cell cell;
		String excelRow = null;
		String cellValue = null;
		
		for (int i = 0; i < intCol; i++) {
			cell = row.getCell(i);
			if (cell!=null) cellValue = getCellValueAsString(cell.getCellTypeEnum(),cell).trim();
			else cellValue = "";
			if (excelRow!=null) excelRow = excelRow + "\t" + cellValue; 
			else excelRow = cellValue;
		}
		return excelRow;
	}
	
	public static String getCellValue (XSSFRow row, Integer intCol) {
		XSSFCell cell;
		cell = row.getCell(intCol);
		return getCellValueAsString(cell.getCellTypeEnum(),cell).trim();
	}
	
    /**
     * This method for the type of data in the cell, extracts the data and
     * returns it as a string.
     */
    public static String getCellValueAsString(CellType type, Cell cell) {
        String cellValue = null;
        if (cell != null) {
            switch (type) {
	            case STRING:
	            	cellValue= cell.getStringCellValue();
	                break;
	            case NUMERIC:
	            	cellValue= String.valueOf((int) cell.getNumericCellValue());
	                break;
	            case BOOLEAN:
	            	cellValue= cell.getStringCellValue();
	                break;
	            case BLANK:
	            	cellValue="";
	                break;
				case ERROR:
					cellValue="";
	                break;
				case FORMULA:
					switch (cell.getCachedFormulaResultTypeEnum()) {
						case STRING:
							cellValue= cell.getStringCellValue();
			                break;
			            case NUMERIC:
			            	cellValue= String.valueOf((int) cell.getNumericCellValue());
			                break;
						default:
							cellValue="";
							break;
					}
	                break;
				case _NONE:
					cellValue="";
	                break;
				default:
					cellValue="";
	                break;
	            }
        }
//        System.out.println(cellValue);
        return cellValue;
    }
    
    /**
	 * This method add Data Validation to an Excel sheet
	 * 
	 */
	public static void addExcelDataValidation(XSSFSheet excelSheet, String[] values, int startRow, int endRow, int startCol, int endCol) {
		XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(excelSheet);
		XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint)
		dvHelper.createExplicitListConstraint(values);
		CellRangeAddressList addressList = new CellRangeAddressList(1, endRow, 0, 0);
		XSSFDataValidation validation = (XSSFDataValidation)dvHelper.createValidation(dvConstraint, addressList);
		validation.setShowErrorBox(true);
		excelSheet.addValidationData(validation);
	}
	
	/**
	 * This method set Excel headers as filters
	 * 
	 */
	public static void addExcelHeaderFilter(XSSFSheet excelSheet, int row, int startCol, int endCol) {
		CellRangeAddress excelRange = new CellRangeAddress(row, row, startCol, endCol);
		
		excelSheet.setAutoFilter(excelRange);
	}

	/**
	 * This method fills an Excel Range with a color
	 * 
	 */
	public static void addExcelHeaderCellStyle(XSSFWorkbook excelWkb, XSSFSheet excelSheet, java.awt.Color cellColor, int fontColor, boolean isRequired,
			int row, int startCol, int endCol) {
		XSSFCellStyle cellStyle = excelWkb.createCellStyle();
		cellStyle.setBorderLeft(BorderStyle.THIN);
		cellStyle.setBorderRight(BorderStyle.THIN);
		cellStyle.setBorderTop(BorderStyle.THIN);
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setFillForegroundColor(new XSSFColor(cellColor));
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		
		Font cellFont = excelWkb.createFont();
		cellFont.setBold(true);
		if (isRequired) cellFont.setColor(IndexedColors.DARK_RED.getIndex());
		else cellFont.setColor((short)fontColor);
		cellStyle.setFont(cellFont);

         Row excelRow = excelSheet.getRow(row);
         for(int c = startCol; c <= endCol; c++){
	        Cell cell = excelRow.getCell(c);
	        cell.setCellStyle(cellStyle);
    	}
	}
	
    /**
	 * This method write and closes an Excel Workbooks
	 * 
	 */
	public static void finishExcelWorkBook(XSSFWorkbook excelWkb, String outputFileName) {
		try {	
			FileOutputStream fileOut = new FileOutputStream(outputFileName);
			excelWkb.write(fileOut);
			excelWkb.close();
			fileOut.close();
		} catch (Exception e) {
			CommonUtil.catchException(CommonUtil.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
		}
	}
	
	/**
	 * This method reads the output csv file and triggers the method to generate excel file
	 * 
	 */
	public static void csvRead(String fileName, String delimiter, XSSFWorkbook excelWkb, XSSFSheet excelSheet) {
		try {
			String thisLine;
	        ArrayList<ArrayList<String>> arList = null;
	        ArrayList<String> al = null;
	        File fileDir = new File(fileName);
	
			BufferedReader myInput;
			
				myInput = new BufferedReader(
						new InputStreamReader(
								new FileInputStream(fileDir)));
		
			arList = new ArrayList<ArrayList<String>>();
	        while ((thisLine = myInput.readLine()) != null) {
	            al = new ArrayList<String>();
	            String[] strar = thisLine.split("\\" + delimiter);
	            int j = 0;
	            while (j < strar.length) {
	                al.add(strar[j]);
	                ++j;
	            }
	            arList.add(al);
	        }
	        int listSize = arList.size();
	        ArrayList<ArrayList<String>> tempList = new ArrayList<ArrayList<String>>();
	        int c = 0;
	        while (c < listSize) {
	            tempList.add(arList.get(c));
	            ++c;
	        }
	        generateExcel(tempList, excelWkb, excelSheet);
			myInput.close();
	
		} catch (Exception e) {
			CommonUtil.catchException(CommonUtil.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
		}
	}

	/**
	 * This method generates .xlsx file format from output csv file
	 * 
	 */
	
	public static void generateExcel(ArrayList<ArrayList<String>> arList, XSSFWorkbook excelWkb, XSSFSheet excelSheet) {
		try {
			for (int k = 0; k < arList.size(); k++) {
				ArrayList<String> ardata = arList.get(k);
				XSSFRow excelRow = excelSheet.createRow(k);
	
				for (int p = 0; p < ardata.size(); p++) {
					XSSFCell excelCell = excelRow.createCell(p);
					String data = ardata.get(p).toString();
					excelCell.setCellValue(data);
				}
			}			
		} catch (Exception e) {
			CommonUtil.catchException(CommonUtil.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
		}
	}
	
	public static void finishProgressBarStatus(Boolean successfulRunStatus, String currentActivity, String progressDetails) {		
		CommonVariables.log.info(currentActivity);
		if (successfulRunStatus) CommonVariables.dlgJob.updateProgressBar(100,"Finished", progressDetails);
		else {
			CommonVariables.dlgJob.cancelProgressBar(currentActivity);
		}
	}
	
	public static void updateProgressBarStatus(Integer percentComplete, String progressLabel, 
			String currentActivity, String progressDetails, Boolean threadCountUpdate) {		
		if (!threadCountUpdate)CommonVariables.log.info(currentActivity);
		if (CommonVariables.caughtException) CommonVariables.dlgJob.cancelProgressBar(null);
		if (CommonVariables.caughtError) CommonVariables.dlgJob.cancelProgressBar(currentActivity);
		else {
			if (progressDetails == null) progressDetails = currentActivity;
			CommonVariables.dlgJob.updateProgressBar(percentComplete, progressLabel, progressDetails);	
		}
	}
	
	/**
	 * Delete file.
	 */
	public static void deleteFile(String fileName) {
		File file = new File(fileName); 
		if (file.exists()) {
			file.delete(); 
		}
	}
	
	/**
	 * Delete directory.
	 */
	public static void deleteDirectory(String dirName) {
		File directory = new File(dirName);
		if (directory.isDirectory()) {
			directory.delete();
		}
	}
	
	/**
	 * Waits for futures (load tasks list put to executor) are partially ready.
	 * <code>maxNumberInList</code> parameters specifies how much tasks could be
	 * uncompleted.
	 * 
	 * @param futures - futures to wait for.
	 * 
	 * @param maxNumberInList - maximum number of futures could be left in
	 * "undone" state.
	 * 
	 * @return sum of executed futures execution time.
	 */
	public static long waitForTasksReady(Collection<Future<Long>> futures,	int maxNumberInList) {
		long totalResult = 0l;
		while (futures.size() > maxNumberInList) {
			try {
				Thread.sleep(20);
			} catch (Exception e) {
				// ignore it...
			}
			for (Future<Long> future : new ArrayList<Future<Long>>(futures)) {
				if (future.isDone()) {
					try {
						totalResult += future.get();
						futures.remove(future);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		return totalResult;
	}
	
	/**
	 * Show Message Window
	 */
	public static void showErrorMessage(String title, String message, Logger log) {
		JOptionPane.showMessageDialog(null, message, 
				title, JOptionPane.ERROR_MESSAGE);
		log.warn(message);
	}
	
	/**
	 * Catch Exception
	 */
	public synchronized static void catchException(String errorLocation, Exception e, Logger log) {
		e.printStackTrace();
		if (!CommonVariables.caughtException) {	
			eDate = new Date();	
			if (log != null) log.error("Exception (" + e.toString() + ") at " + dateFormat.format(eDate));	
			final CatchException showException = new CatchException(errorLocation + "\n\n" + getStackTrace(e));
			showException.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			CommonVariables.jobStopped = true;
			CommonVariables.caughtException = true;
			if (CommonVariables.dlgJob !=null) CommonVariables.dlgJob.cancelProgressBar("The following error has occurred: \n" + e.toString());
			showException.setVisible(true);	
		}
	}
	
	/**
	 * Retrieve the Exception Details
	 */
	public synchronized static String getStackTrace(final Throwable throwable) {
	     final StringWriter sw = new StringWriter();
	     final PrintWriter pw = new PrintWriter(sw, true);
	     throwable.printStackTrace(pw);
	     return sw.getBuffer().toString();
	}
	
	/**
	 * Record record Worker Exceptions
	 */
	public synchronized static void recordThreadException(Exception e, BufferedWriter failedBufferedWriter, String data) {
    	try {
    		e.printStackTrace();
    		eDate = new Date();
    		CommonVariables.log.info("Exception (" + e.toString() + ") at " + dateFormat.format(eDate) + ": " + data);	
			failedBufferedWriter.append(e.toString() + "\t" + data + BD_Constants.NEW_LINE_SEPARATOR);
			failedBufferedWriter.flush();
		} catch (Exception e1) {
			CommonUtil.catchException("Could not write failed record to file.", e1, CommonVariables.log);
		}
    }
    
	/**
	 * Record record Worker Errors
	 */
    public synchronized static void recordThreadError(String errorMessage, BufferedWriter failedBufferedWriter, String data) {
    	try {
    		eDate = new Date();
//    		CommonVariables.log.info(errorMessage + " : " + data);	
			failedBufferedWriter.append(errorMessage + "\t" + data + BD_Constants.NEW_LINE_SEPARATOR);
			failedBufferedWriter.flush();
		} catch (Exception e) {
			CommonUtil.catchException("Could not write failed record to file.", e, CommonVariables.log);
		}
    }
	
	/**
	 * Catch Error
	 *
	 */
	public static void waitForCancelConfirmation() {
		while (CommonVariables.confirmingCancel) {
			try {
				TimeUnit.MILLISECONDS.sleep(1000);
			} catch (InterruptedException e) {
				CommonUtil.catchException("Could not enable sleep.", e, CommonVariables.log);
			}
		} 
	}
}
