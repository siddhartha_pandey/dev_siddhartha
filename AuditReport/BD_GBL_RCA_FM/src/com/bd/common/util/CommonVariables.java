package com.bd.common.util;

import java.awt.*;
import java.util.*;

import org.apache.log4j.Logger;

import com.bd._main.GUI.JobDialog;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.reltio.cst.service.ReltioAPIService;

public class CommonVariables {
	public static String mainPropertiesFile = "properties/main.properties";
	public static String auditObjectsPropertiesFile = "properties/auditObjectTypes.properties";
	public static String credentialsPropertiesFile = "properties/credentials.properties";
	public static ReltioAPIService reltioAPIService;
	public static Logger log;
	public static JobDialog dlgJob;
	public static boolean validCredentials = false;	
	public static boolean isAdmin = false;
	public static boolean jobStopped = false;
	public static boolean jobCancelled = false;
	public static boolean jobFinished = false;
	public static boolean confirmingCancel = false;
	public static boolean caughtException = false;
	public static boolean caughtError = false;
	public static String userID;
	public static String userPassword;
	public static String authUrl;
	public static String tokenUrl;
	public static String reltioEnvironment;
	public static String tenantID;;
	public static String tenantUrl;
	public static String propertiesEnvironments;
	public static String auditReportFileName;
	public static String failedFileName;
	public static String delimiter;
	public static Integer scanLimit;
	public static Integer threadCount;
	public static ArrayList<String> listPropertiesAuditGroups = new ArrayList<String>(); 
	public static ArrayList<String> listPropertiesAuditObjects = new ArrayList<String>(); 
	public static Multimap <String,String> propertiesAuditObjectsMap = ArrayListMultimap.create();
	public static ArrayList<String> userTenantList = new ArrayList<String>();    
	public static ArrayList<String> userRoleList = new ArrayList<String>();   
	public static ArrayList<String> usersList = new ArrayList<String>();   
	public static String [] propertiesEnvironmentsList = null;
	public static String[] objectTypes = new String[] {"null"};
	public static java.util.List<String> validRoles = new ArrayList<String>();
	
	public static Color colorBlue = new java.awt.Color(91,155,213);
	public static Color colorGreen = new java.awt.Color(112,173,71);
	public static Color colorGrey = new java.awt.Color(165,165,165);
	public static Color colorRed = new java.awt.Color(192,0,0);
	
	public static void testCredentials() {
		userID = "";
		userPassword = "";
		reltioEnvironment = "QA";	//DEV, QA, PROD
		authUrl = "https://auth.reltio.com/oauth";
		tokenUrl = authUrl + "/token";
	}
}
