package com.bd.common.util;

public interface BD_Constants {
    public static final String INPUT_DELIMITER = "\\|\\|";
	public static final String CSV_SEPARATOR = ";";
	public static final String TEXT_DELIMETER = ",";
	public static final String NEW_LINE_SEPARATOR = "\n";
	public static final String REGEX_COURSE_TRANSFORMATION = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
}
