package com.bd.common.util;

import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import com.google.gson.*;

public class UserLists {
	public static void getAllUserList() {
		try {
			Gson gson = new Gson();
			String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
			
			String apiGetUsers  = CommonVariables.authUrl + "/users/tenant/" +  CommonVariables.tenantID;
			String usersResponse = CommonUtil.apiGetRequest(apiGetUsers, methodName);

			JsonArray userListArray = gson.fromJson(usersResponse, JsonElement.class).getAsJsonArray();
			for (int i = 0; i < userListArray.size(); i++) {
				JsonObject userListObject = userListArray.get(i).getAsJsonObject();

				if (userListObject.has("userPermissions")) {
					JsonObject userPermissionsObject = userListObject.get("userPermissions").getAsJsonObject();

					if (userPermissionsObject.has("roles")) {
						JsonObject rolesObject = userPermissionsObject.get("roles").getAsJsonObject();

						if (rolesObject != null) {
							Set<Entry<String, JsonElement>> roleSet = rolesObject.entrySet();
							for (Map.Entry<String, JsonElement> role : roleSet) {
								if (role.getKey().toString().contains("FM")) {
									if (!CommonVariables.usersList.contains(userListObject.get("username").getAsString())) {
										CommonVariables.usersList.add(userListObject.get("username").getAsString());
									}
								} else if (role.getKey().toString().contains("SERVICE")) {
									if (!CommonVariables.usersList.contains(userListObject.get("username").getAsString())) {
										CommonVariables.usersList.add(userListObject.get("username").getAsString());
									}
								}
							}
						}
					}
				}

				if (userListObject.has("groups")) {

					JsonArray userGroupsArray = userListObject.get("groups").getAsJsonArray();
					if (userGroupsArray != null) {
						for (int userGroupCount = 0; userGroupCount < userGroupsArray.size(); userGroupCount++) {
							String userGroupString = userGroupsArray.get(userGroupCount).getAsString();
							if (userGroupString.contains("FM")) {
								if (!CommonVariables.usersList.contains(userListObject.get("username").getAsString())) {
									CommonVariables.usersList.add(userListObject.get("username").getAsString());
								}
							} else if (userGroupString.contains("SERVICE")) {
								if (!CommonVariables.usersList.contains(userListObject.get("username").getAsString())) {
									CommonVariables.usersList.add(userListObject.get("username").getAsString());
								}
							} else if (userGroupString.contains("DEVELOPERS")) {
								if (!CommonVariables.usersList.contains(userListObject.get("username").getAsString())) {
									CommonVariables.usersList.add(userListObject.get("username").getAsString());
								}
							}
						}
					}
				}
			}
			
			CommonVariables.usersList.sort(String::compareToIgnoreCase);  
			

		} catch (Exception ex) {
			ex.printStackTrace();
			CommonVariables.log.error(ex.getMessage());
		}
	}
}
