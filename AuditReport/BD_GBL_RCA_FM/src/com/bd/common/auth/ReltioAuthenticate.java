package com.bd.common.auth;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;

import com.google.gson.*;
import com.reltio.cst.service.*;
import com.reltio.cst.service.impl.*;
import com.bd.common.util.CommonUtil;
import com.bd.common.util.CommonVariables;

/*
 * this Class "ReltioAuthenticate" will check given credential is correct or not.
 */
public class ReltioAuthenticate {

	private SecretKeySpec secretKey;
	private Cipher cipher;
	public static ReltioAuthenticate ske = null;
	public static File dir = null;

	/*
	 * this constructor "ReltioAuthenticate" is used to initialize secretKey and
	 * cipher .
	 */
	public ReltioAuthenticate(String secret, int length, String algorithm)
			throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException {
		byte[] key = new byte[length];
		key = fixSecret(secret, length);
		this.secretKey = new SecretKeySpec(key, algorithm);
		this.cipher = Cipher.getInstance(algorithm);
	}
	/*
	 * this function "createReltioAPIService" will check given User credential is
	 * correct or not.
	 */

	public static Boolean createReltioAPIService(String userName, String password, String tokenUrl, Logger log) {
		try {
			ReltioAPIService appReltioAPIService = null;
			TokenGeneratorService tokenGeneratorService;
			tokenGeneratorService = new TokenGeneratorServiceImpl(userName, password, tokenUrl);
			appReltioAPIService = new SimpleReltioAPIServiceImpl(tokenGeneratorService);
			CommonVariables.reltioAPIService = appReltioAPIService;
			log.info("User credentials confirmed.");
			log.info("Username: " + CommonVariables.userID);
			return true;
		} catch (Exception e) {
			log.warn("Access Denied. Invalid user credentials.");
//			CommonUtil.catchException("TEST" + ReltioAuthenticate.class.getSimpleName() 
//					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, log);
		}
		return false;
	}

	/*
	 * this function "createServiceReltioAPIService" will check given credential for
	 * Reporting is correct or not.
	 */
	public static Boolean createServiceReltioAPIService(String tokenUrl, Logger log) {
		try {
			InputStream iNS = new FileInputStream(CommonVariables.credentialsPropertiesFile);
			dir = new File("properties/Encryptions.txt");
			OutputStream oTS = new FileOutputStream(dir);
			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = iNS.read(bytes)) != -1) {
				oTS.write(bytes, 0, read);
			}
			oTS.flush();

			iNS.close();
			oTS.close();

			ReltioAuthenticate ske = null;
			try {
				ske = new ReltioAuthenticate("$3@R#!k#&P@&s#@r0", 16, "AES");
				try {
					ske.encryptData(dir);
				} catch (Exception e) {
					System.err.println("Couldn't decrypt " + dir.getName() + ": " + e.getMessage());
				}
			} catch (UnsupportedEncodingException ex) {
				System.err.println("Couldn't create key: " + ex.getMessage());
			} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
				System.err.println(e.getMessage());
			}

			String[] uP = ReltioAuthenticate.getUP(ske).split("#");
			String usernameReporting = uP[0];
			String passwordReporting = uP[1];

//			System.out.println(usernameReporting + " ==> " + passwordReporting);

			ReltioAPIService appReltioAPIService = null;
			TokenGeneratorService tokenGeneratorService;
			tokenGeneratorService = new TokenGeneratorServiceImpl(usernameReporting, passwordReporting, tokenUrl);
			appReltioAPIService = new SimpleReltioAPIServiceImpl(tokenGeneratorService);
			CommonVariables.reltioAPIService = appReltioAPIService;
			log.info("Reporting credentials confirmed.");
//			log.info("Username: "+ CommonVariables.userID);
			return true;
		} catch (Exception e) {
			log.warn("Access Denied. Invalid user credentials.");
//			CommonUtil.catchException("TEST" + ReltioAuthenticate.class.getSimpleName() 
//					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, log);
		}
		return false;
	}

	public static Boolean validateCredentials(String userName, String password, String environment, String authUrl,
			String tokenUrl, Logger log) {
		try {
			CommonVariables.reltioAPIService = null;
			String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
			JsonObject objUserRoles = null;
			JsonArray arrayUserGroups = null;

			if (createReltioAPIService(userName, password, tokenUrl, log)) {
				// Pull User Credentials
				String apiGetUserDetails = authUrl + "/users/" + userName;
				String apiResponse = CommonUtil.apiGetRequest(apiGetUserDetails, methodName);
				if (apiResponse != null) {
					Gson gson = new Gson();
					JsonObject objUser = gson.fromJson(apiResponse, JsonObject.class);

					if (objUser.get("groups") != null) { // get user Groups
						arrayUserGroups = objUser.get("groups").getAsJsonArray();
					}

					if (objUser.get("userPermissions") != null) { // get user Permissions
						JsonObject objUserPermissions = objUser.get("userPermissions").getAsJsonObject();
						if (objUserPermissions != null) {
							if (objUserPermissions.get("roles") != null) { // get user Roles
								objUserRoles = objUserPermissions.get("roles").getAsJsonObject();
							} else {
								CommonUtil.showErrorMessage("Access Denied", "User Roles unavailable.", log);
								return false;
							}
						}
					} else {
						CommonUtil.showErrorMessage("Access Denied", "User Permissions unavailable.", log);
						return false;
					}

					if (validAccess(environment, objUserRoles, arrayUserGroups, log)) {
						if (!getObjectTypes(log))
							return false; // get Audit Object Types
						return true;
					} else
						CommonUtil.showErrorMessage("Access Denied",
								"User does not have the access to use this app for Reltio tenant ("
										+ CommonVariables.tenantID + ").",
								log);
				} else
					CommonUtil.showErrorMessage("Access Denied", "User is missing required role (ROLE_USER).", log);
			} else
				CommonUtil.showErrorMessage("Invalid User Credentials", "The username / password is invalid.", log);
		} catch (Exception e) {
			CommonUtil.catchException(ReltioAuthenticate.class.getSimpleName() + "."
					+ Thread.currentThread().getStackTrace()[1].getMethodName(), e, log);
		}
		return false;
	}

	public static Boolean validAccess(String environment, JsonObject objUserRoles, JsonArray arrayUserGroups,
			Logger log) {

		Map<String, List<String>> map = new HashMap<String, List<String>>();

		String rolesAndGroups = ("ROLE_BD_FM_FOS_RO|ROLE_BD_FM_FOS_DS|ROLE_BD_FM_FOS_SU|"
				+ "ROLE_ADMIN_TENANT|ROLE_BD_SERVICE|ROLE_BD_READONLY|"
				+ "FM_FOS_RO|FM_FOS_SU|FM_FOS_DS|BD_DEVELOPERS|AM_DEVELOPERS|FM_GBL_DEVELOPERS");

		try {
			Properties prop = new Properties();
			InputStream input = null;
			input = new FileInputStream(CommonVariables.mainPropertiesFile);
			prop.load(input);

			String tenantList = prop.getProperty("TENANTS");
			String[] tenantListArray = tenantList.trim().split(",");

			for (String tenants : tenantListArray) {
				String[] individualTenants = tenants.trim().split("_");

				List<String> valGlobal = new ArrayList<String>();
				valGlobal.add(individualTenants[1]);
				valGlobal.add(individualTenants[0]);
				valGlobal.add(rolesAndGroups);

				// put values into map
				if (individualTenants[0].equalsIgnoreCase("dev")) {
					map.put("DEV", valGlobal);
				} else if (individualTenants[0].equalsIgnoreCase("test")) {
					map.put("QA", valGlobal);
				} else {
					map.put("PROD", valGlobal);
				}
			}

			/*
			 * List<String> valGlobalDev = new ArrayList<String>();
			 * valGlobalDev.add("zpTTftjBvvTA91E"); valGlobalDev.add("dev");
			 * valGlobalDev.add(rolesAndGroups);
			 * 
			 * List<String> valGlobalQa = new ArrayList<String>();
			 * valGlobalQa.add("ion3SHV0kWJ59Es"); valGlobalQa.add("test");
			 * valGlobalQa.add(rolesAndGroups);
			 * 
			 * List<String> valGlobalProd = new ArrayList<String>();
			 * valGlobalProd.add("Eab8HWG8cdQfx4z"); valGlobalProd.add("360");
			 * valGlobalProd.add(rolesAndGroups);
			 * 
			 * // put values into map map.put("DEV", valGlobalDev); map.put("QA",
			 * valGlobalQa); map.put("PROD", valGlobalProd);
			 */

		} catch (Exception ex) {
			CommonUtil.catchException(ReltioAuthenticate.class.getSimpleName() + "."
					+ Thread.currentThread().getStackTrace()[1].getMethodName(), ex, log);
		}

		String frmValues = environment;
//        System.out.println(frmValues);
		List<String> tenantDetails = map.get(frmValues);

		CommonVariables.tenantID = tenantDetails.get(0);
		String reltioEnv = tenantDetails.get(1);
		String enivronmentRoles = tenantDetails.get(2);
		String[] parsedEnvironmentRoles = enivronmentRoles.split("\\|");

//        System.out.println(Arrays.toString(parsedEnivronmentRoles));
//        System.out.println(objUserRoles.toString());

		Boolean validTenantAccess = false;

		if (arrayUserGroups != null) {
			int len = arrayUserGroups.size();
			for (int i = 0; i < len; i++) {
//        			System.out.println(arrayUserRole.get(i));
				String userGroup = arrayUserGroups.get(i).getAsString();
				if (enivronmentRoles.contains(userGroup.trim())) {
					validTenantAccess = true;
					CommonVariables.validRoles.add(userGroup);
//        				break;
				}
			}
		}

		if (!validTenantAccess) {
			if (objUserRoles != null) {
				for (String environmentRole : parsedEnvironmentRoles) {
					if (objUserRoles.get(environmentRole) != null) {
						JsonArray arrayUserRole = objUserRoles.get(environmentRole).getAsJsonArray();
						int len = arrayUserRole.size();
						for (int i = 0; i < len; i++) {
							// System.out.println(arrayUserRole.get(i));
							String userTenant = arrayUserRole.get(i).getAsString();
							if (userTenant.equalsIgnoreCase(CommonVariables.tenantID)) {
								validTenantAccess = true;
								CommonVariables.validRoles.add(environmentRole);
								// break;
							}
						}
					}
				}
			}
		}

		if (validTenantAccess) {
			CommonVariables.tenantUrl = "https://" + reltioEnv.toLowerCase() + ".reltio.com/reltio/api/"
					+ CommonVariables.tenantID;
			log.info("Reltio Environment Details: " + frmValues + " (" + CommonVariables.tenantUrl + ")");
			return true;
		}
		return false;
	}

	/**
	 * Load Audit Object Types variables
	 */
	public static Boolean getObjectTypes(Logger log) {
		String fileLine = null;
		try (BufferedReader reader = new BufferedReader(
				new InputStreamReader(new FileInputStream(CommonVariables.auditObjectsPropertiesFile), "UTF8"))) {
			while ((fileLine = reader.readLine()) != null) {
				String[] fileLineValues = null;
				fileLineValues = fileLine.split(";");

				String object = fileLineValues[0].trim();
				String item = fileLineValues[1].trim();
				CommonVariables.propertiesAuditObjectsMap.put(object, item);

				if (!CommonVariables.listPropertiesAuditGroups.contains(object)) {
					CommonVariables.listPropertiesAuditGroups.add(object);
				}

			}
			reader.close();
			return true;
		} catch (Exception e) {
			if (e instanceof FileNotFoundException) {
				CommonUtil.showErrorMessage("Application Error",
						"Properties file '" + CommonVariables.auditObjectsPropertiesFile + "' not found.", log);
			} else
				CommonUtil.catchException(ReltioAuthenticate.class.getSimpleName() + "."
						+ Thread.currentThread().getStackTrace()[1].getMethodName(), e, log);
		}
		return false;
	}

	private byte[] fixSecret(String s, int length) throws UnsupportedEncodingException {
		if (s.length() < length) {
			int missingLength = length - s.length();
			for (int i = 0; i < missingLength; i++) {
				s += " ";
			}
		}
		return s.substring(0, length).getBytes("UTF-8");
	}

	public void encryptData(File f)
			throws InvalidKeyException, IOException, IllegalBlockSizeException, BadPaddingException {
		// System.out.println("Encrypting file: " + f.getName());
		this.cipher.init(Cipher.ENCRYPT_MODE, this.secretKey);
		this.writeToFile(f);
	}

	public void decryptData(File f)
			throws InvalidKeyException, IOException, IllegalBlockSizeException, BadPaddingException {
		// System.out.println("Decrypting file: " + f.getName());
		this.cipher.init(Cipher.DECRYPT_MODE, this.secretKey);
		this.writeToFile(f);
	}

	public void writeToFile(File f) throws IOException, IllegalBlockSizeException, BadPaddingException {
		FileInputStream in = new FileInputStream(f);
		byte[] input = new byte[(int) f.length()];
		in.read(input);

		FileOutputStream out = new FileOutputStream(f);
		byte[] output = this.cipher.doFinal(input);
		out.write(output);

		out.flush();
		out.close();
		in.close();
	}

	public static String getUP(ReltioAuthenticate ske) {

		String data = "";
		try {
			ske.decryptData(dir);
			BufferedReader dbr = new BufferedReader(new FileReader(dir));
			String[] dataArray = dbr.readLine().split("#");
			String dU = dataArray[0].toString();
			String dP = dataArray[1].toString();

			String u = "";
			char[] cDU = dU.toCharArray();
			for (int i = 0; i < cDU.length; i++) {
				if (i % 5 == 0) {
					u += cDU[i];
				}
			}

			String p = "";
			char[] cDP = dP.toCharArray();
			for (int i = 0; i < cDP.length; i++) {
				if (i % 5 == 0) {
					p += cDP[i];
				}
			}
			dbr.close();
			data = u + "#" + p;

		} catch (Exception e) {
			e.printStackTrace();
			e.getMessage();
		}
		return data;
	}
}
