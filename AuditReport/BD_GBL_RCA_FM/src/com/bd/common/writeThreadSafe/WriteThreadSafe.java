package com.bd.common.writeThreadSafe;

import java.io.BufferedWriter;

import com.bd.common.util.*;

public class  WriteThreadSafe {
	public synchronized static Boolean write(BufferedWriter bufferedWriter, String data){
		try {
			bufferedWriter.write(data);
			bufferedWriter.flush();
			return true;
		} catch (Exception e) {
			CommonUtil.catchException(WriteThreadSafe.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
		}
		return false;
	}
}
