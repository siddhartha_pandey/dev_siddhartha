package com.bd.common.processor;

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

import org.apache.poi.xssf.usermodel.*;

import com.bd.common.util.*;

public class ExcelProcessor implements BD_Constants {
	public final static AtomicLong processedFileLineCount = new AtomicLong();
	public final static AtomicLong failedCount = new AtomicLong();
	public final static AtomicLong headerCount = new AtomicLong();
	public final static AtomicLong totalLineCount = new AtomicLong();
	public final static AtomicLong externalProcessableCounter = new AtomicLong();
	
	private Boolean isPrimaryProcessor;
	private Boolean isSingleExecutor;
	
	private BufferedWriter outputBufferedWriter;
	private BufferedWriter outputBufferedWriter2;
	private BufferedWriter failedBufferedWriter;

	private Integer jobNumber;
	private String inputFileName;
	private List<String> mandatoryHeaders;
	
	private XSSFWorkbook excelWkb;
	private XSSFSheet excelSheet;
	private XSSFRow headerRow;
	private XSSFRow dataRow;
	private XSSFCell excelCell;
	
	private int rowCount; 
	private int columnCount;
	private String headerData;
	private String rowData;
	 
	public ExcelProcessor(String inputFileName, List<String> mandatoryHeaders, 
			BufferedWriter failedBufferedWriter, int jobNumber, 
			boolean isPrimaryProcessor, boolean isSingleExecutor) {
		this.inputFileName = inputFileName;
		this.mandatoryHeaders = mandatoryHeaders;
		this.failedBufferedWriter = failedBufferedWriter;
		this.jobNumber = jobNumber;
		this.isPrimaryProcessor = isPrimaryProcessor;
		this.isSingleExecutor = isSingleExecutor;
	}
	
	public ExcelProcessor(String inputFileName, List<String> mandatoryHeaders, 
			BufferedWriter outputBufferedWriter, BufferedWriter failedBufferedWriter, int jobNumber, 
			boolean isPrimaryProcessor, boolean isSingleExecutor) {
		this.inputFileName = inputFileName;
		this.mandatoryHeaders = mandatoryHeaders;
		this.outputBufferedWriter = outputBufferedWriter;
		this.failedBufferedWriter = failedBufferedWriter;
		this.jobNumber = jobNumber;
		this.isPrimaryProcessor = isPrimaryProcessor;
		this.isSingleExecutor = isSingleExecutor;
	}
	
	public ExcelProcessor(String inputFileName, List<String> mandatoryHeaders, 
			BufferedWriter outputBufferedWriter, BufferedWriter outputBufferedWriter2, 
			BufferedWriter failedBufferedWriter, int jobNumber, 
			boolean isPrimaryProcessor, boolean isSingleExecutor) {
		this.inputFileName = inputFileName;
		this.mandatoryHeaders = mandatoryHeaders;
		this.outputBufferedWriter = outputBufferedWriter;
		this.outputBufferedWriter2 = outputBufferedWriter2;
		this.failedBufferedWriter = failedBufferedWriter;
		this.jobNumber = jobNumber;
		this.isPrimaryProcessor = isPrimaryProcessor;
		this.isSingleExecutor = isSingleExecutor;
	}

	public void runProcess() {			
		processedFileLineCount.set(0);
		failedCount.set(0);
		headerCount.set(0);
		totalLineCount.set(0);
		externalProcessableCounter.set(0);
		
		CommonUtil.waitForCancelConfirmation();
		if (CommonVariables.jobStopped) return;
		openInputFile(inputFileName); 
		
		if (!CommonUtil.validInputSheets(excelWkb)) return;	
		if (!validInputHeaders()) return;	
		if (!getFileLineCount()) return;
		
		CommonUtil.waitForCancelConfirmation();
		if (CommonVariables.jobStopped) return;
		readInputFileLines ();	
		CommonUtil.closeExcelWorkbook(excelWkb);
	}
	
	private void openInputFile(String inputFileName) {
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		
		if (isPrimaryProcessor) CommonUtil.updateProgressBarStatus(3, null, "Opening input file...", null, false);
		FileInputStream fileInputStream = CommonUtil.createFileInputStream(inputFileName);
		if(fileInputStream==null) {	
			CommonUtil.updateProgressBarStatus(null, "Stopping", "Failed to create an input file stream.", null, false);
			return;
		}
			
		excelWkb = CommonUtil.openExcelWorkbook(fileInputStream);
		if(excelWkb ==null){
			CommonUtil.updateProgressBarStatus(null, "Stopping", "Failed to open the Excel workbook.", null, false);
			CommonUtil.closeFileInputStream(fileInputStream, methodName);
		}
	}
	
	private Boolean validInputHeaders()	{
		String currentCellValue = null; 
		
		if (isPrimaryProcessor) CommonUtil.updateProgressBarStatus(4, null, "Checking for mandatory fields...", null, false);
		try {
			excelSheet = excelWkb.getSheetAt(0);
			headerRow = excelSheet.getRow(0);
			columnCount = headerRow.getPhysicalNumberOfCells();
	
			List<String> listFileHeaders = new ArrayList<String>();
			
		    for(int c = 0; c < columnCount; c++) {	//Find header fields from input file
		    	currentCellValue = null;
		        excelCell = headerRow.getCell((short)c);
		        if(excelCell != null) {
		        	currentCellValue = excelCell.getStringCellValue().trim();
		        	listFileHeaders.add(currentCellValue.toUpperCase());
		        }
		    }
		    
		    for (String mandatoryHeader : mandatoryHeaders) {	// Look for mandatory fields within the input file header list
		    	mandatoryHeader = mandatoryHeader.toUpperCase();
		    	if (!listFileHeaders.contains(mandatoryHeader)) {
					CommonVariables.caughtError = true;
					CommonVariables.jobStopped = true;
					CommonUtil.updateProgressBarStatus(null, "Stopping", "Mandatory header '" + mandatoryHeader + "' is missing from the input file.", null, false);
		    		return false;
				}
			}
		    
		    CommonVariables.log.info("Mandatory header fields found.");
		    headerCount.set(1);
		    headerData = CommonUtil.getExcelRow(headerRow, columnCount);
		    
		    if (isPrimaryProcessor) {
			    failedBufferedWriter.append("ERROR_REASON\t" + headerData + BD_Constants.NEW_LINE_SEPARATOR);
		    	failedBufferedWriter.flush();
		    }
	    	
	    	if (outputBufferedWriter!=null) {
	    		outputBufferedWriter.append(headerData + BD_Constants.NEW_LINE_SEPARATOR);
	    		outputBufferedWriter.flush();
	    	}
	    	
	    	if (outputBufferedWriter2!=null) {
	    		outputBufferedWriter2.append(headerData + BD_Constants.NEW_LINE_SEPARATOR);
	    		outputBufferedWriter2.flush();
		    }
	    	
	    	return true;
		}
		catch (Exception e) {
			CommonUtil.catchException(ExcelProcessor.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
		} 
	    return false;
	}
	
	private Boolean getFileLineCount() {
		rowCount = excelSheet.getPhysicalNumberOfRows();	
	    totalLineCount.set(rowCount);
	    CommonVariables.log.info("File Lines: " + NumberFormat.getInstance().format(totalLineCount));
	    if (rowCount<2) {
			CommonVariables.caughtError = true;
			CommonVariables.jobStopped = true;
			CommonUtil.updateProgressBarStatus(null, "Stopping", "No records detected in the file", null, false);
    		return false;
		}
	    return true;
	}

	private void readInputFileLines() {
		if (isPrimaryProcessor) updateProgress(5, "Reading input file lines...", false);
		try {
			List<String> arrayFileLines = new ArrayList<String>();

			for(int r = 0; r < rowCount; r++) {
	    		CommonUtil.waitForCancelConfirmation();
	    		if (CommonVariables.jobStopped) return;
	    		
	     		dataRow = excelSheet.getRow(r);
		        if(dataRow != null) {
		        	rowData = CommonUtil.getExcelRow(dataRow, columnCount);		
		        	if (!headerData.equalsIgnoreCase(rowData)) {        	
		        		arrayFileLines.add(rowData);		        	
		        	}			        	
		        } else {
		        	failedCount.incrementAndGet();
		        	failedBufferedWriter.append("Row has no data.\t" + "File Line #: " + dataRow + BD_Constants.NEW_LINE_SEPARATOR);
			    	failedBufferedWriter.flush();
		        }
		        
		        if (arrayFileLines.size() == CommonVariables.scanLimit) {
	    			parallelProcessing(arrayFileLines);
	    			arrayFileLines.clear();
	    		}	 
		    }	
			if (arrayFileLines.size()>0) {	//finishes processing the last batch size
				parallelProcessing(arrayFileLines);
				arrayFileLines.clear();
			}		
		}
		catch (Exception e) {
			CommonUtil.catchException(ExcelProcessor.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
		} 
	}
	
	/**
	 * This method sends processes the scanned objects through a thread executor.
	 */
	private void parallelProcessing(List<String> arrayFileLines) { 	
		ExecutorService executor;
		if (!isSingleExecutor) executor = Executors.newFixedThreadPool(CommonVariables.threadCount);
		else executor = Executors.newSingleThreadExecutor();
		Runnable worker = null;
		
		for(int currentFileLine = 0; currentFileLine<arrayFileLines.size();currentFileLine++) {
			if (CommonVariables.jobStopped) break;
       	
			rowData = arrayFileLines.get(currentFileLine);
        	if (isPrimaryProcessor) externalProcessableCounter.incrementAndGet();			
        	
        	switch (jobNumber) { 
				case 1:

					break;
				case 2:

					break;
				case 3:

					break;
				case 4:

					break;
			}	
			if (worker != null) executor.execute(worker);
		}
		executor.shutdown();
		
		while (!executor.isTerminated()) {
			if (CommonVariables.jobStopped) executor.shutdownNow();
			updateProgress(null, "Processing records...", true);
		}	
	}

	private synchronized void updateProgress(Integer percentComplete, String currentActivity, boolean isThreadCountUpdate) {
		switch (jobNumber) { 
			case 1:
				
				break;
			case 2:
				
				break;
			case 3:
				
				break;
			case 4:
				
				break;
		}	
	}
}
