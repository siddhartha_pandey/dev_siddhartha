package com.bd.common.processor;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

import com.bd.common.util.*;
import com.bd.jobs.AuditReport.Initiator_AuditReport;
import com.bd.jobs.AuditReport.ThreadController_AuditReport;
import com.google.gson.*;

public class AuditProcessor implements BD_Constants {
	public static List<String> listPublishedCustomers = new ArrayList<String>();	
	public final static AtomicLong scannedObjects = new AtomicLong();
	public final static AtomicLong processedObjectsCount = new AtomicLong();
	public final static AtomicLong successObjectsCount = new AtomicLong();
	public final static AtomicLong successRecordsCount = new AtomicLong();
	public final static AtomicLong failedCount = new AtomicLong();
	public final static AtomicLong activityGrouping = new AtomicLong();;
	
	private final int MAX_QUEUE_SIZE_MULTIPLICATOR = 1;
	
	private Integer jobNumber;
	private String queryFilter;
	
	public AuditProcessor(String queryFilter, int jobNumber) {
		this.queryFilter = queryFilter;
		this.jobNumber = jobNumber;
	}
	
	public void runProcess() {
		listPublishedCustomers = new ArrayList<String>();
		scannedObjects.set(0);
		processedObjectsCount.set(0);
		successObjectsCount.set(0);
		successRecordsCount.set(0);
		failedCount.set(0);

		CommonUtil.waitForCancelConfirmation();
		if (CommonVariables.jobStopped) return;

		long scanTotal = getScanTotal();
		if (scanTotal > 0) {
			runSearch();
		} else if (scanTotal == 0) CommonUtil.updateProgressBarStatus(null, "Stopping", "Scan total = 0.", null, false);
	}

	/**
	 * This method returns the total count of the scan.
	 */
	private long getScanTotal() {
		Gson gson = new Gson();
		String totalResponse = null;
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String decodedQueryFilter = null;
		
		CommonVariables.log.info("Query Filter: " + queryFilter);
		decodedQueryFilter = CommonUtil.decodeString(queryFilter, methodName);
		if (decodedQueryFilter == null) {
			CommonUtil.updateProgressBarStatus(null, "Stopping", "Unable to decode the query filter.", null, false);
			return -1;
		}
		
		String apiGetTotal  = CommonVariables.tenantUrl + "/activities/_total?" + decodedQueryFilter;
//		System.out.println(apiGetTotal);
		totalResponse = CommonUtil.apiGetRequest(apiGetTotal, methodName);
		if (totalResponse == null) {
			CommonUtil.updateProgressBarStatus(null, "Stopping", "Unable to retrieve the Total API response.", null, false);
			return -1;
		}

		JsonObject objTotal = gson.fromJson(totalResponse, JsonObject.class);
		if (objTotal.get("total") == null) {
			CommonUtil.updateProgressBarStatus(null, "Stopping", "Unable to retrieve the scan total.", null, false);
			return -1;
		}
		
		long valTotal = objTotal.get("total").getAsLong();
		scannedObjects.set(valTotal);
		CommonVariables.log.info("Total Audit Log Count: " + scannedObjects);
//		System.out.println("Total Audit Log Count: " + scannedObjects);
		return valTotal;	
	}
	
	/**
	 * This method runs the Scan API.
	 */
	private void runSearch() {
		Integer count = 0;
		Integer offset = 0;
		String apiGetSearch;
		String scanResponse;
		Gson gson = new Gson();
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String decodedQueryFilter;			
				
		decodedQueryFilter = CommonUtil.decodeString(queryFilter, methodName);
		if (decodedQueryFilter == null) {
			CommonUtil.updateProgressBarStatus(null, "Stopping", "Unable to decode the query filter.", null, false);
			return;
		}
		
		CommonUtil.waitForCancelConfirmation();
		if (CommonVariables.jobCancelled) return;
		
		updateProgress(5, "Starting search...", false);

		
		ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors.newFixedThreadPool(CommonVariables.threadCount);
		boolean eof = false;
		ArrayList<Future<Long>> futures = new ArrayList<Future<Long>>();

		while (!eof && !CommonVariables.jobStopped) {
			for (int threadNum = 0; threadNum < CommonVariables.threadCount * MAX_QUEUE_SIZE_MULTIPLICATOR; threadNum++) {
				// Doing the Seacrh API Call here
				apiGetSearch = CommonVariables.tenantUrl + "/activities?" + decodedQueryFilter 
						+ " &max=" + CommonVariables.scanLimit + "&offset=" + offset;
//				System.out.println(apiGetSearch);
				scanResponse = CommonUtil.apiGetRequest(apiGetSearch, methodName);
//				System.out.println(scanResponse);
				// Convert the string to java object
				JsonArray arraySearchResponse = gson.fromJson(scanResponse, JsonElement.class).getAsJsonArray();

				if (arraySearchResponse.size() > 0) {
					count += arraySearchResponse.size();
					for (int i = 0; i < arraySearchResponse.size(); i++) {
						JsonObject auditObject = arraySearchResponse.get(i).getAsJsonObject();
						futures.add(executorService.submit(new Callable<Long>() {
							@Override
							public Long call() throws Exception {
								updateProgress(null, "Processing records...", true);
								long requestExecutionTime = 0l;
								try {
									long startTime = System.currentTimeMillis();
									
									switch (jobNumber) { 
										case 1:
//											System.out.println(auditObjects.toString());										
											final ThreadController_AuditReport newAuditObjectThreadRun = 
													new ThreadController_AuditReport(auditObject, activityGrouping.incrementAndGet());
											newAuditObjectThreadRun.run();
											break;
										case 2:
											break;
									}
	
									// Not used as of now for performance
									// evaluation. can be used in future.
									requestExecutionTime = System.currentTimeMillis() - startTime;
								} catch (Exception e) {
									CommonUtil.catchException(AuditProcessor.class.getSimpleName() 
											+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
								}
								return requestExecutionTime;
							}
						}));
					}
					CommonVariables.log.info("Scanned record count >>> " + count);
					arraySearchResponse=null;
					offset += CommonVariables.scanLimit;
				} else {
					eof = true;
					break;
				}	
			}
			CommonUtil.waitForTasksReady(futures, CommonVariables.threadCount * (MAX_QUEUE_SIZE_MULTIPLICATOR / 2));
		}

		CommonUtil.waitForTasksReady(futures, 0);
		executorService.shutdown();

		if (!CommonVariables.jobStopped) {
			updateProgress(null, "Processing completed...", false);
			return;
		}
	}
	
	private synchronized void updateProgress(Integer percentComplete, String currentActivity, boolean isThreadCountUpdate) {
		switch (jobNumber) { 
			case 1:
				if (isThreadCountUpdate) percentComplete = Initiator_AuditReport.progressBarPercent();
				CommonUtil.updateProgressBarStatus(percentComplete, null, currentActivity, 
						Initiator_AuditReport.progressBarDetails(currentActivity), isThreadCountUpdate);
				break;
			case 2:
				break;
		}	
	}
}
