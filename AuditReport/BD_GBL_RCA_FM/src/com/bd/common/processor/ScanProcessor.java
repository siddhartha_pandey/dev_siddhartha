package com.bd.common.processor;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

import com.bd.common.bo.ReltioObject;
import com.bd.common.util.*;
import com.bd.jobs.AuditReport.Initiator_AuditReport;
import com.google.gson.*;

public class ScanProcessor implements BD_Constants {
	public static List<String> listPublishedCustomers = new ArrayList<String>();	
	public final static AtomicLong scannedEntities = new AtomicLong();
	public final static AtomicLong processedEntitiesCount = new AtomicLong();
	public final static AtomicLong successEntitiesCount = new AtomicLong();
	public final static AtomicLong successRecordsCount = new AtomicLong();
	public final static AtomicLong failedCount = new AtomicLong();
	
	private final int MAX_QUEUE_SIZE_MULTIPLICATOR = 1;
	
	private Integer jobNumber;
	private String queryFilter;
	private String scanParameters;
	
	public ScanProcessor(String queryFilter, String scanParameters, int jobNumber) {
		this.queryFilter = queryFilter;
		this.scanParameters = scanParameters;
		this.jobNumber = jobNumber;
	}
	
	public void runProcess() {
		listPublishedCustomers = new ArrayList<String>();
		scannedEntities.set(0);
		processedEntitiesCount.set(0);
		successEntitiesCount.set(0);
		successRecordsCount.set(0);
		failedCount.set(0);

		CommonUtil.waitForCancelConfirmation();
		if (CommonVariables.jobStopped) return;

		long scanTotal = getScanTotal();
		if (scanTotal > 0) {
			runScan();
		} else if (scanTotal == 0) CommonUtil.updateProgressBarStatus(null, "Stopping", "Scan total = 0.", null, false);
	}

	/**
	 * This method returns the total count of the scan.
	 */
	private long getScanTotal() {
		Gson gson = new Gson();
		String totalResponse = null;
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String decodedQueryFilter = null;
		
		CommonVariables.log.info("Query Filter: " + queryFilter);
		decodedQueryFilter = CommonUtil.decodeString(queryFilter, methodName);
		if (decodedQueryFilter == null) {
			CommonUtil.updateProgressBarStatus(null, "Stopping", "Unable to decode the query filter.", null, false);
			return -1;
		}
		
		String apiGetTotal  = CommonVariables.tenantUrl + "/entities/_total?" + decodedQueryFilter;
		totalResponse = CommonUtil.apiGetRequest(apiGetTotal, methodName);
		if (totalResponse == null) {
			CommonUtil.updateProgressBarStatus(null, "Stopping", "Unable to retrieve the Total API response.", null, false);
			return -1;
		}

		JsonObject objTotal = gson.fromJson(totalResponse, JsonObject.class);
		if (objTotal.get("total") == null) {
			CommonUtil.updateProgressBarStatus(null, "Stopping", "Unable to retrieve the scan total.", null, false);
			return -1;
		}
		
		long valTotal = objTotal.get("total").getAsLong();
		scannedEntities.set(valTotal);
		CommonVariables.log.info("Total Entities Count: " + scannedEntities);
		return valTotal;	
	}
	
	/**
	 * This method runs the Scan API.
	 */
	private void runScan() {
		Integer count = 0;
		String apiPostScan;
		String apiPostScanBody;
		String apiPostScanCursor;
		String scanResponse;
		Gson gson = new Gson();
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		String decodedQueryFilter;			
				
		decodedQueryFilter = CommonUtil.decodeString(queryFilter, methodName);
		if (decodedQueryFilter == null) {
			CommonUtil.updateProgressBarStatus(null, "Stopping", "Unable to decode the query filter.", null, false);
			return;
		}
		
		CommonUtil.waitForCancelConfirmation();
		if (CommonVariables.jobCancelled) return;
		
		updateProgress(5, "Starting scan...", false);
		apiPostScan = CommonVariables.tenantUrl + "/entities/_scan?" + decodedQueryFilter + scanParameters;
		apiPostScanBody = null;
		
		ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors.newFixedThreadPool(CommonVariables.threadCount);
		boolean eof = false;
		ArrayList<Future<Long>> futures = new ArrayList<Future<Long>>();

		while (!eof && !CommonVariables.jobStopped) {
			for (int threadNum = 0; threadNum < CommonVariables.threadCount * MAX_QUEUE_SIZE_MULTIPLICATOR; threadNum++) {
				// Doing the DBScan API Call here
				scanResponse = CommonUtil.apiPostRequest(apiPostScan, apiPostScanBody, methodName);
				// Convert the string to java object
				ScanResponse scanResponseObj = gson.fromJson(scanResponse, ScanResponse.class);

				if (scanResponseObj.getObjects() != null && scanResponseObj.getObjects().size() > 0) {
					count += scanResponseObj.getObjects().size();
					final List<ReltioObject> objectsToProcess = scanResponseObj.getObjects();

					for (ReltioObject reltioObject : objectsToProcess) {
						futures.add(executorService.submit(new Callable<Long>() {
							@Override
							public Long call() throws Exception {
								updateProgress(null, "Processing records...", true);
								long requestExecutionTime = 0l;
								try {
									long startTime = System.currentTimeMillis();
									
									switch (jobNumber) { 
										case 1:
											System.out.println(reltioObject);
//											final ThreadController_AuditReport newMatchesThreadRun = 
//													new ThreadController_AuditReport(reltioObject);
//											newMatchesThreadRun.run();
											break;
										case 2:
											break;
									}
	
									// Not used as of now for performance
									// evaluation. can be used in future.
									requestExecutionTime = System.currentTimeMillis() - startTime;
								} catch (Exception e) {
									CommonUtil.catchException(ScanProcessor.class.getSimpleName() 
											+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
								}
								return requestExecutionTime;
							}
						}));
					}
					CommonVariables.log.info("Scanned record count >>> " + count);

				} else {
					eof = true;
					break;
				}
				scanResponseObj.setObjects(null);
				apiPostScanCursor = gson.toJson(scanResponseObj.getCursor());
//				System.out.println("Cursor:" + apiPostScanCursor);
				apiPostScanBody = "{\"cursor\":" + apiPostScanCursor + "}";
			}
			CommonUtil.waitForTasksReady(futures, CommonVariables.threadCount * (MAX_QUEUE_SIZE_MULTIPLICATOR / 2));
		}

		CommonUtil.waitForTasksReady(futures, 0);
		executorService.shutdown();

		if (!CommonVariables.jobStopped) {
			updateProgress(null, "Processing completed...", false);
			return;
		}
	}
	
	private synchronized void updateProgress(Integer percentComplete, String currentActivity, boolean isThreadCountUpdate) {
		switch (jobNumber) { 
			case 1:
				if (isThreadCountUpdate) percentComplete = Initiator_AuditReport.progressBarPercent();
				CommonUtil.updateProgressBarStatus(percentComplete, null, currentActivity, 
						Initiator_AuditReport.progressBarDetails(currentActivity), isThreadCountUpdate);
				break;
			case 2:
				break;
		}	
	}
}
