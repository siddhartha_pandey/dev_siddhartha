package com.bd.common.processor;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

import com.bd.common.util.*;

public class TextProcessor implements BD_Constants {
	public final static AtomicLong processedFileLineCount = new AtomicLong();
	public final static AtomicLong failedCount = new AtomicLong();
	public final static AtomicLong headerCount = new AtomicLong();
	public final static AtomicLong totalLineCount = new AtomicLong();
	public final static AtomicLong externalProcessableCounter = new AtomicLong();
	
	private Boolean isPrimaryProcessor;
	private Boolean isSingleExecutor;
	
	private BufferedWriter outputBufferedWriter;
	private BufferedWriter outputBufferedWriter2;
	private BufferedWriter failedBufferedWriter;

	private Integer jobNumber;
	private String inputFileName;
	private List<String> mandatoryHeaders;
	
	private BufferedReader bufferedReader;
	private String headerRow;
	private String dataRow;
	 
	public TextProcessor(String inputFileName, List<String> mandatoryHeaders, 
			BufferedWriter failedBufferedWriter, int jobNumber, 
			boolean isPrimaryProcessor, boolean isSingleExecutor) {
		this.inputFileName = inputFileName;
		this.mandatoryHeaders = mandatoryHeaders;
		this.failedBufferedWriter = failedBufferedWriter;
		this.jobNumber = jobNumber;
		this.isPrimaryProcessor = isPrimaryProcessor;
		this.isSingleExecutor = isSingleExecutor;
	}
	
	public TextProcessor(String inputFileName, List<String> mandatoryHeaders, 
			BufferedWriter outputBufferedWriter, BufferedWriter failedBufferedWriter, 
			int jobNumber, boolean isPrimaryProcessor, boolean isSingleExecutor) {
		this.inputFileName = inputFileName;
		this.mandatoryHeaders = mandatoryHeaders;
		this.outputBufferedWriter = outputBufferedWriter;
		this.failedBufferedWriter = failedBufferedWriter;
		this.jobNumber = jobNumber;
		this.isPrimaryProcessor = isPrimaryProcessor;
		this.isSingleExecutor = isSingleExecutor;
	}
	
	public TextProcessor(String inputFileName, List<String> mandatoryHeaders, 
			BufferedWriter outputBufferedWriter, BufferedWriter outputBufferedWriter2, 
			BufferedWriter failedBufferedWriter, int jobNumber, 
			boolean isPrimaryProcessor, boolean isSingleExecutor) {
		this.inputFileName = inputFileName;
		this.mandatoryHeaders = mandatoryHeaders;
		this.outputBufferedWriter = outputBufferedWriter;
		this.outputBufferedWriter2 = outputBufferedWriter2;
		this.failedBufferedWriter = failedBufferedWriter;
		this.jobNumber = jobNumber;
		this.isPrimaryProcessor = isPrimaryProcessor;
		this.isSingleExecutor = isSingleExecutor;
	}

	public void runProcess() {			
		processedFileLineCount.set(0);
		failedCount.set(0);
		headerCount.set(0);
		totalLineCount.set(0);
		externalProcessableCounter.set(0);
		
		CommonUtil.waitForCancelConfirmation();
		if (CommonVariables.jobStopped) return;
		openInputFile(inputFileName); 
		
		if (!validInputHeaders()) return;	
		
		CommonUtil.waitForCancelConfirmation();
		if (CommonVariables.jobStopped) return;
		readInputFileLines ();	
		
		CommonUtil.closeBufferedReader(bufferedReader);
	}
	
	private void openInputFile(String inputFileName) {
		
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		
		if (isPrimaryProcessor) CommonUtil.updateProgressBarStatus(3, null, "Opening input file...", null, false);
		FileInputStream fileInputStream = CommonUtil.createFileInputStream(inputFileName);
		if(fileInputStream==null) {	
			CommonUtil.updateProgressBarStatus(null, "Stopping", "Failed to create an input file stream.", null, false);
			return;
		}
		
		bufferedReader = CommonUtil.createBufferedReader(fileInputStream);
		if(bufferedReader ==null){
			CommonUtil.updateProgressBarStatus(null, "Stopping", "Failed to open the Excel workbook.", null, false);
			CommonUtil.closeFileInputStream(fileInputStream, methodName);
		}
	}
	
	private Boolean validInputHeaders()	{
		
		if (isPrimaryProcessor) CommonUtil.updateProgressBarStatus(4, null, "Checking for mandatory fields...", null, false);
		try {
			headerRow = bufferedReader.readLine().toString().toUpperCase();
			String inputDelimiter = "\t";
	        String[] headerValues = null;
	        headerValues = headerRow.split(inputDelimiter);		
			List<String> listFileHeaders = Arrays.asList(headerValues);
		    
		    for (String mandatoryHeader : mandatoryHeaders) {	// Look for mandatory fields within the input file header list
		    	mandatoryHeader.toUpperCase();
		    	if (!listFileHeaders.contains(mandatoryHeader)) {
					CommonVariables.caughtError = true;
					CommonVariables.jobStopped = true;
					CommonUtil.updateProgressBarStatus(null, "Stopping", "Mandatory header '" + mandatoryHeader + "' is missing from the input file.", null, false);
		    		return false;
				}
			}
		    
		    CommonVariables.log.info("Mandatory header fields found.");
		    headerCount.set(1);
		    
		    if (isPrimaryProcessor) {
		    	failedBufferedWriter.append("ERROR_REASON\t" + headerRow + BD_Constants.NEW_LINE_SEPARATOR);
		    	failedBufferedWriter.flush();
		    }

	    	if (outputBufferedWriter!=null) {
	    		outputBufferedWriter.append(headerRow + BD_Constants.NEW_LINE_SEPARATOR);
	    		outputBufferedWriter.flush();
	    	}
	    	
	    	if (outputBufferedWriter2!=null) {
	    		outputBufferedWriter2.append(headerRow + BD_Constants.NEW_LINE_SEPARATOR);
	    		outputBufferedWriter2.flush();
	    	}
	    	
	    	return true;
		}
		catch (Exception e) {
			CommonUtil.catchException(TextProcessor.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
		} 
	    return false;
	}

	private void readInputFileLines() {
		
		if (isPrimaryProcessor) updateProgress(5, "Reading input file lines...", false);
		try {
			int batchSize = CommonVariables.scanLimit;
			List<String> arrayFileLines = new ArrayList<String>();
		    
			while ((dataRow = bufferedReader.readLine()) != null) {
	    		CommonUtil.waitForCancelConfirmation();
	    		if (CommonVariables.jobStopped) return;
	    		
	    		if (!headerRow.equalsIgnoreCase(dataRow)) arrayFileLines.add(dataRow);		        		        
	    		
	    		if (arrayFileLines.size() == batchSize) {
	    			parallelProcessing(arrayFileLines);
	    			arrayFileLines.clear();
	    			updateProgress(null, "Scanning for more records...", false);
	    		}	
		    }	
			if (arrayFileLines.size()>0) {	//finishes processing the last batch size
				parallelProcessing(arrayFileLines);
				arrayFileLines.clear();
				updateProgress(null, "Processing completed...", false);
			}	
		}
		catch (Exception e) {
			CommonUtil.catchException(TextProcessor.class.getSimpleName() 
					+ "." + Thread.currentThread().getStackTrace()[1].getMethodName(), e, CommonVariables.log);
		} 
	}	
	
	/**
	 * This method sends processes the scanned objects through a thread executor.
	 */
	private void parallelProcessing(List<String> arrayFileLines) { 	

		ExecutorService executor;
		if (!isSingleExecutor) executor = Executors.newFixedThreadPool(CommonVariables.threadCount);
		else executor = Executors.newSingleThreadExecutor();
		Runnable worker = null;
		
		for(int currentFileLine = 0; currentFileLine<arrayFileLines.size();currentFileLine++) {
			if (CommonVariables.jobStopped) break;
       	
			dataRow = arrayFileLines.get(currentFileLine);
        	if (isPrimaryProcessor) externalProcessableCounter.incrementAndGet();					        	

			switch (jobNumber) { 
				case 1:
					break;
			}	
	    	if (worker != null) executor.execute(worker);		
		}
		executor.shutdown();
		
		while (!executor.isTerminated()) {
			if (CommonVariables.jobStopped) executor.shutdownNow();
			updateProgress(null, "Processing records...", true);
		}	
	}
	
	private void updateProgress(Integer percentComplete, String currentActivity, boolean isThreadCountUpdate) {
		switch (jobNumber) { 
			case 1:
				break;

		}	
	}
}
