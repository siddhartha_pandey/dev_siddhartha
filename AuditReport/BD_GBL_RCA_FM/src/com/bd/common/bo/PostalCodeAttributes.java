package com.bd.common.bo;

import java.util.List;

public class PostalCodeAttributes {
	private List<Attribute> SecondaryPostalCode;
	public List<Attribute> getSecondaryPostalCode() {
		return SecondaryPostalCode;
	}
	public void setSecondaryPostalCode(List<Attribute> SecondaryPostalCode) {
		this.SecondaryPostalCode = SecondaryPostalCode;
	}

	private List<Attribute> FullPostalCode;
	public List<Attribute> getFullPostalCode() {
		return FullPostalCode;
	}
	public void setPostalCode(List<Attribute> PostalCode) {
		this.FullPostalCode = PostalCode;
	}

	private List<Attribute> PrimaryPostalCode;
	public List<Attribute> getPrimaryPostalCode() {
		return PrimaryPostalCode;
	}
	public void setPrimaryPostalCode(List<Attribute> PrimaryPostalCode) {
		this.PrimaryPostalCode = PrimaryPostalCode;
	}
}
