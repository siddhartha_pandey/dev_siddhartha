package com.bd.common.bo;

import java.util.List;

public class CustomerAttributes {
	
	public List<Location> Address;
    public List<Location> getAddress() {
		return Address;
	}
	public void setAddress(List<Location> address) {
		Address = address;
	}
	
	public List<SalesOrganization> SalesOrganization;
    public List<SalesOrganization> getSalesOrganization() {
		return SalesOrganization;
	}
	public void setSalesOrganization(List<SalesOrganization> salesOrganization) {
		SalesOrganization = salesOrganization;
	}

	private List<Attribute> GagID;
    public List<Attribute> getGagID() {
        return GagID;
    }
    public void setGagID(List<Attribute> GagID) {
        this.GagID = GagID;
    }

    private List<Attribute> NielsenID;
    public List<Attribute> getNielsenID() {
        return NielsenID;
    }
    public void setNielsenID(List<Attribute> NielsenID) {
        this.NielsenID = NielsenID;
    }

    private List<Attribute> CustomerClassification;
    public List<Attribute> getCustomerClassification() {
        return CustomerClassification;
    }
    public void setCustomerClassification(List<Attribute> CustomerClassification) {
        this.CustomerClassification = CustomerClassification;
    }

    private List<Attribute> CustomerFunction;
    public List<Attribute> getCustomerFunction() {
        return CustomerFunction;
    }
    public void setCustomerFunction(List<Attribute> CustomerFunction) {
        this.CustomerFunction = CustomerFunction;
    }
    
    private List<Attribute> PartnerFunction;
    public List<Attribute> getPartnerFunction() {
        return PartnerFunction;
    }
    public void setPartnerFunction(List<Attribute> PartnerFunction) {
        this.PartnerFunction = PartnerFunction;
    }
    
    private List<Attribute> PartnerFlag;
    public List<Attribute> getPartnerFlag() {
        return PartnerFlag;
    }
    public void setPartnerFlag(List<Attribute> PartnerFlag) {
        this.PartnerFlag = PartnerFlag;
    }
    
    private List<Attribute> CentralDeletionFlag;
    public List<Attribute> getCentralDeletionFlag() {
        return CentralDeletionFlag;
    }
    public void setCentralDeletionFlag(List<Attribute> CentralDeletionFlag) {
        this.CentralDeletionFlag = CentralDeletionFlag;
    }

    private List<Attribute> CentralOrderBlock;
    public List<Attribute> getCentralOrderBlock() {
        return CentralOrderBlock;
    }
    public void setCentralOrderBlock(List<Attribute> CentralOrderBlock) {
        this.CentralOrderBlock = CentralOrderBlock;
    }
    
    private List<Attribute> CreateDate;
    public List<Attribute> getCreateDate() {
        return CreateDate;
    }
    public void setCreateDate(List<Attribute> CreateDate) {
        this.CreateDate = CreateDate;
    }

    private List<Attribute> IndustryCode;
    public List<Attribute> getIndustryCode() {
        return IndustryCode;
    }
    public void setIndustryCode(List<Attribute> IndustryCode) {
        this.IndustryCode = IndustryCode;
    }

    private List<Attribute> Name;
    public List<Attribute> getName() {
        return Name;
    }
    public void setName(List<Attribute> Name) {
        this.Name = Name;
    }

    private List<Attribute> AccountGroup;
    public List<Attribute> getAccountGroup() {
        return AccountGroup;
    }
    public void setAccountGroup(List<Attribute> AccountGroup) {
        this.AccountGroup = AccountGroup;
    }
    
    private List<Attribute> AccountNumber;
    public List<Attribute> getAccountNumber() {
		return AccountNumber;
	}
	public void setAccountNumber(List<Attribute> accountNumber) {
		AccountNumber = accountNumber;
	}

	private List<Attribute> CustomerNumber;
    public List<Attribute> getCustomerNumber() {
        return CustomerNumber;
    }
    public void setCustomerNumber(List<Attribute> CustomerNumber) {
        this.CustomerNumber = CustomerNumber;
    }
    
	private List<Attribute> CustomerID;
    public List<Attribute> getCustomerID() {
        return CustomerID;
    }
    public void setCustomerID(List<Attribute> CustomerID) {
        this.CustomerID = CustomerID;
    }
    
	public List<Attribute> LOTSCustomerNumber;
	public List<Attribute> getLOTSCustomerNumber() {
		return LOTSCustomerNumber;
	}
	public void setLOTSCustomerNumber(List<Attribute> lotsCustomerNumber) {
		LOTSCustomerNumber = lotsCustomerNumber;
	}
	
	public List<Attribute> LOTSMasterBE;
	public List<Attribute> getLOTSMasterBE() {
		return LOTSMasterBE;
	}
	public void setLOTSMasterBE(List<Attribute> lotsMasterBE) {
		LOTSMasterBE = lotsMasterBE;
	}
	
	public List<Attribute> LOTSBESequence;
	public List<Attribute> getLOTSBESequence() {
		return LOTSBESequence;
	}
	public void setLOTSBESequence(List<Attribute> lotsBESequence) {
		LOTSBESequence = lotsBESequence;
	}	
    
    private List<Attribute> DistributionChannel;
    public List<Attribute> getDistributionChannel() {
        return DistributionChannel;
    }
    public void setDistributionChannel(List<Attribute> DistributionChannel) {
        this.DistributionChannel = DistributionChannel;
    }

    private List<Attribute> Division;
    public List<Attribute> getDivision() {
        return Division;
    }
    public void setDivision(List<Attribute> Division) {
        this.Division = Division;
    }

    private List<Attribute> TrainStation;
    public List<Attribute> getTrainStation() {
        return TrainStation;
    }
    public void setTrainStation(List<Attribute> TrainStation) {
        this.TrainStation = TrainStation;
    }

    private List<Attribute> SalesOrg;
    public List<Attribute> getSalesOrg() {
        return SalesOrg;
    }
    public void setSalesOrg(List<Attribute> SalesOrg) {
        this.SalesOrg = SalesOrg;
    }

    private List<Attribute> CustomerGroup;
    public List<Attribute> getCustomerGroup() {
        return CustomerGroup;
    }
    public void setCustomerGroup(List<Attribute> CustomerGroup) {
        this.CustomerGroup = CustomerGroup;
    }

    private List<Attribute> Department;
    public List<Attribute> getDepartment() {
        return Department;
    }
    public void setDepartment(List<Attribute> Department) {
        this.Department = Department;
    }

    private List<Attribute> Name1;
    public List<Attribute> getName1() {
        return Name1;
    }
    public void setName1(List<Attribute> Name1) {
        this.Name1 = Name1;
    }
    
    private List<Attribute> Name2;
    public List<Attribute> getName2() {
        return Name2;
    }
    public void setName2(List<Attribute> Name2) {
        this.Name2 = Name2;
    }
    
    private List<Attribute> Name3;
    public List<Attribute> getName3() {
        return Name3;
    }
    public void setName3(List<Attribute> Name3) {
        this.Name3 = Name3;
    }
    
    private List<Attribute> Street;
    public List<Attribute> getStreet() {
        return Street;
    }
    public void setStreet(List<Attribute> Street) {
        this.Street = Street;
    }
    
    private List<Attribute> Street3;
    public List<Attribute> getStreet3() {
        return Street3;
    }
    public void setStreet3(List<Attribute> Street3) {
        this.Street3 = Street3;
    }
    
    private List<Attribute> MatchName;
    public List<Attribute> getMatchName() {
        return MatchName;
    }
    public void setMatchName(List<Attribute> MatchName) {
        this.MatchName = MatchName;
    }
    
    private List<Attribute> FullName;
    public List<Attribute> getFullName() {
        return FullName;
    }
    public void setFullName(List<Attribute> FullName) {
        this.FullName = FullName;
    }
    
    private List<Attribute> MatchPriority;
    public List<Attribute> getMatchPriority() {
        return MatchPriority;
    }
    public void setMatchPriority(List<Attribute> MatchPriority) {
        this.MatchPriority = MatchPriority;
    }
    
    private List<Attribute> MatchIndex;
    public List<Attribute> getMatchIndex() {
        return MatchIndex;
    }
    public void setMatchIndex(List<Attribute> MatchIndex) {
        this.MatchIndex = MatchIndex;
    }
    
    private List<Attribute> EverestDowngradeFlag;
    public List<Attribute> getEverestDowngradeFlag() {
        return EverestDowngradeFlag;
    }
    public void setEverestDowngradeFlag(List<Attribute> EverestDowngradeFlag) {
        this.EverestDowngradeFlag = EverestDowngradeFlag;
    }
    
    private List<Attribute> EverestDropShipFlag;
    public List<Attribute> getEverestDropShipFlag() {
        return EverestDropShipFlag;
    }
    public void setEverestDropShipFlag(List<Attribute> EverestDropShipFlag) {
        this.EverestDropShipFlag = EverestDropShipFlag;
    }
    
    private List<Attribute> EverestIntercompanyFlag;
    public List<Attribute> getEverestIntercompanyFlag() {
        return EverestIntercompanyFlag;
    }
    public void setEverestIntercompanyFlag(List<Attribute> EverestIntercompanyFlag) {
        this.EverestIntercompanyFlag = EverestIntercompanyFlag;
    }
    
    private List<Attribute> ReadyForMatchFlag;
    public List<Attribute> getReadyForMatchFlag() {
        return ReadyForMatchFlag;
    }
    public void setReadyForMatchFlag(List<Attribute> ReadyForMatchFlag) {
        this.ReadyForMatchFlag = ReadyForMatchFlag;
    }
    
    private List<Attribute> VerificationStatus;
	public List<Attribute> getVerificationStatus() {
		return VerificationStatus;
	}
	public void setVerificationStatus(List<Attribute> verificationStatus) {
		VerificationStatus = verificationStatus;
	}
	   
	public List<Attribute> Source;
	public List<Attribute> getSource() {
		return Source;
	}
	public void setSource(List<Attribute> source) {
		Source = source;
	}
	
	private List<ASCC> BDDSASCCOverride;
	public List<ASCC> getBDDSASCCOverride() {
		return BDDSASCCOverride;
	}
	public void setBDDSASCCOverride(List<ASCC> bDDSASCCOverride) {
		BDDSASCCOverride = bDDSASCCOverride;
	}
    
	private List<ASCC> BDBASCCOverride;
	public List<ASCC> getBDBASCCOverride() {
		return BDBASCCOverride;
	}
	public void setBDBASCCOverride(List<ASCC> bDBASCCOverride) {
		BDBASCCOverride = bDBASCCOverride;
	}
	
	private List<ASCC> PASASCCOverride;
	public List<ASCC> getPASASCCOverride() {
		return PASASCCOverride;
	}
	public void setPASASCCOverride(List<ASCC> pASASCCOverride) {
		PASASCCOverride = pASASCCOverride;
	}
	
	private List<ASCC>  MPSASCCOverride;
	public List<ASCC> getMPSASCCOverride() {
		return MPSASCCOverride;
	}
	public void setMPSASCCOverride(List<ASCC> mPSASCCOverride) {
		MPSASCCOverride = mPSASCCOverride;
	}
	
	private List<ASCC> HDSASCCOverride;
	public List<ASCC> getHDSASCCOverride() {
		return HDSASCCOverride;
	}
	public void setHDSASCCOverride(List<ASCC> hDSASCCOverride) {
		HDSASCCOverride = hDSASCCOverride;
	}

}