package com.bd.common.bo;

public class Location {
	
	String label;
    public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	private LocationAttributes attributes;
    public LocationAttributes getAttributes() {
        return attributes;
    }
    public void setAttributes(LocationAttributes attributes) {
        this.attributes = attributes;
    }

    private String uri;
    public String getUri() {
        return uri;
    }
    public void setUri(String uri) {
        this.uri = uri;
    }

    private LocationAttributes value;
    public LocationAttributes getValue() {
        return value;
    }
    public void setValue(LocationAttributes value) {
        this.value = value;
    }
    public Boolean ov;

	public Boolean getOv() {
		return ov;
	}

	public void setOv(Boolean ov) {
		this.ov = ov;
	}
    
	public Boolean isOv() {
		return ov;
	}
}
