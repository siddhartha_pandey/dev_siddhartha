package com.bd.common.bo;

import java.util.List;

public class CleanserPostalCodeAttributes {
    private List<Attribute> CleanserSecondaryPostalCode;
    public List<Attribute> getCleanserSecondaryPostalCode() {
        return CleanserSecondaryPostalCode;
    }
    public void setCleanserSecondaryPostalCode(List<Attribute> CleanserSecondaryPostalCode) {
        this.CleanserSecondaryPostalCode = CleanserSecondaryPostalCode;
    }

    private List<Attribute> CleanserFullPostalCode;
    public List<Attribute> getCleanserFullPostalCode() {
        return CleanserFullPostalCode;
    }
    public void setCleanserFullPostalCode(List<Attribute> CleanserFullPostalCode) {
        this.CleanserFullPostalCode = CleanserFullPostalCode;
    }

    private List<Attribute> CleanserPrimaryPostalCode;
    public List<Attribute> getCleanserPrimaryPostalCode() {
        return CleanserPrimaryPostalCode;
    }
    public void setCleanserPrimaryPostalCode(List<Attribute> CleanserPrimaryPostalCode) {
        this.CleanserPrimaryPostalCode = CleanserPrimaryPostalCode;
    }
}


