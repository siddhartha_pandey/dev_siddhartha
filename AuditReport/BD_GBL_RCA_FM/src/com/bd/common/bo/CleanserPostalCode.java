package com.bd.common.bo;

import java.util.List;

public class CleanserPostalCode {
    private CleanserPostalCodeAttributes value;
    public CleanserPostalCodeAttributes getValue() {
        return value;
    }
    public void setValue(CleanserPostalCodeAttributes value) {
        this.value = value;
    }

    private String type;
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    private String uri;
    public String getUri() {
        return uri;
    }
    public void setUri(String uri) {
        this.uri = uri;
    }

    private String createdTime;
    public String getCreatedTime() {
        return createdTime;
    }
    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    private String updatedTime;
    public String getUpdatedTime() {
        return updatedTime;
    }
    public void setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
    }

    private String relationshipLabel;
    public String getRelationshipLabel() {
        return relationshipLabel;
    }
    public void setRelationshipLabel(String relationshipLabel) {
        this.relationshipLabel = relationshipLabel;
    }

    private List<Attribute> crosswalks;
    public List<Attribute> getCrosswalks() {
        return crosswalks;
    }
    public void setCrosswalks(List<Attribute> crosswalks) {
        this.crosswalks = crosswalks;
    }

    private Crosswalk refEntity;
    public Crosswalk getRefEntity() {
        return refEntity;
    }
    public void setRefEntity(Crosswalk refEntity) {
        this.refEntity = refEntity;
    }

    private Crosswalk refRelation;
    public Crosswalk getRefRelation() {
        return refRelation;
    }
    public void setRefRelation(Crosswalk refRelation) {
        this.refRelation = refRelation;
    }
    
    public Boolean ov;
    public Boolean getOv() {
		return ov;
	}
	public void setOv(Boolean ov) {
		this.ov = ov;
	}
    
	public Boolean isOv() {
		return ov;
	}
}
