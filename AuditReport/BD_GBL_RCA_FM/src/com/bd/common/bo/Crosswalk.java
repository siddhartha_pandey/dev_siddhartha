package com.bd.common.bo;

import java.util.List;

public class Crosswalk {
	public String value;
    public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	private String createdTime;
    public String getCreatedTime()    {
        return createdTime;
    }
    public void setCreatedTime(String createdTime)    {
        this.createdTime = createdTime;
    }
    
    private String uri;
    public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	
	private String updatedTime;
    public String getUpdatedTime()    {
        return updatedTime;
    }
    public void setUpdatedTime(String updatedTime)    {
        this.updatedTime = updatedTime;
    }

    public String type;
    public String getType()    {
        return type;
    }
    public void setType(String type)    {
        this.type = type;
    }

    private String values;
    public String getValues()    {
        return values;
    }
    public void setValues(String values)    {
        this.values = values;
    }

    private String objectURI;
    public String getObjectURI()    {
        return objectURI;
    }
    public void setObjectURI(String objectURI)    {
        this.objectURI = objectURI;
    }

    private List<Attribute> Crosswalks;
    public List<Attribute> getCrosswalks()    {
        return Crosswalks;
    }
    public void setCrosswalks(List<Attribute> Crosswalks)    {
        this.Crosswalks = Crosswalks;
    }
    
    private List<String> attributes;
	public List<String> getAttributes() {
		return attributes;
	}
	public void setAttributes(List<String> attributes) {
		this.attributes = attributes;
	}
	
	public String ownerType;
	public String getOwnerType() {
		return ownerType;
	}
	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}
	
	public String reltioLoadDate;
	public String getReltioLoadDate() {
		return reltioLoadDate;
	}
	public void setReltioLoadDate(String reltioLoadDate) {
		this.reltioLoadDate = reltioLoadDate;
	}
	
	public String updateDate;
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
}
