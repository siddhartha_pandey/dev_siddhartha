package com.bd.common.bo;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class LocationAttributes
 {
	private List<Attribute> AddressType;
	public List<Attribute> getAddressType() {
		return AddressType;
	}
	public void setAddressType(List<Attribute> addressType) {
		AddressType = addressType;
	}

	private List<Attribute> VerificationStatusDetails;
	public List<Attribute> getVerificationStatusDetails() {
		return VerificationStatusDetails;
	}
	public void setVerificationStatusDetails(List<Attribute> VerificationStatusDetails) {
		this.VerificationStatusDetails = VerificationStatusDetails;
	}

	private List<Attribute> CleanserVerificationStatus;
	public List<Attribute> getCleanserVerificationStatus() {
	 return CleanserVerificationStatus;
	}
	public void setCleanserVerificationStatus(List<Attribute> CleanserVerificationStatus) {
		this.CleanserVerificationStatus = CleanserVerificationStatus;
	}

	private List<Attribute> StateProvince;
	public List<Attribute> getStateProvince() {
		return StateProvince;
	}
	public void setStateProvince(List<Attribute> StateProvince) {
		this.StateProvince = StateProvince;
	}
	
	private List<PostalCode> PostalCode;
	public List<PostalCode> getPostalCode() {
		return PostalCode;
	}
	public void setPostalCode(List<PostalCode> PostalCode) {
		this.PostalCode = PostalCode;
	}
	
	@SerializedName(value = "ISO3166-2")
	private List<Attribute> CountryCode;
	public List<Attribute> getCountryCode() {
		return CountryCode;
	}
	public void setCountryCode(List<Attribute> CountryCode) {
		this.CountryCode = CountryCode;
	}

	private List<Attribute> Country;
	public List<Attribute> getCountry() {
		return Country;
	}
	public void setCountry(List<Attribute> Country) {
		this.Country = Country;
	}
	
	private List<Attribute> Premise;
	public List<Attribute> getPremise() {
		return Premise;
	}
	public void setPremise(List<Attribute> Premise) {
		this.Premise = Premise;
	}
 
	private List<Attribute> Thoroughfare;
	public List<Attribute> getThoroughfare() {
		return Thoroughfare;
	}
	public void setThoroughfare(List<Attribute> Thoroughfare) {
		this.Thoroughfare = Thoroughfare;
	}
 
	private List<Attribute> POBox;
	public List<Attribute> getPOBox() {
		return POBox;
	}
	public void setPOBox(List<Attribute> POBox) {
		this.POBox = POBox;
	}
	
	private List<Attribute> SubBuilding;
	public List<Attribute> getSubBuilding() {
		return SubBuilding;
	}
	public void setSubBuilding(List<Attribute> SubBuilding) {
		this.Country = SubBuilding;
	}
	
	private List<Attribute> AddressLine1;
	public List<Attribute> getAddressLine1() {
		return AddressLine1;
	}
	public void setAddressLine1(List<Attribute> AddressLine1) {
		this.AddressLine1 = AddressLine1;
	}
	
	private List<Attribute> MatchAddressLine1;
	public List<Attribute> getMatchAddressLine1() {
		return MatchAddressLine1;
	}
	public void setMatchAddressLine1(List<Attribute> MatchAddressLine1) {
		this.MatchAddressLine1 = MatchAddressLine1;
	}
	
	private List<Attribute> City;
	public List<Attribute> getCity() {
		return City;
	}
	public void setCity(List<Attribute> City) {
		this.City = City;
	}
	
	private List<Attribute> AddressLine2;
 	public List<Attribute> getAddressLine2() {
 		return AddressLine2;
 	}
 	public void setAddressLine2(List<Attribute> AddressLine2) {
 		this.AddressLine2 = AddressLine2;
 	}
 	
 	private List<Attribute> AddressID;
 	public List<Attribute> getAddressID() {
 		return AddressID;
 	}
 	public void setAddressID(List<Attribute> AddressID) {
 		this.AddressID = AddressID;
 	}
 	
 	private List<Attribute> HouseNumber;
 	public List<Attribute> getHouseNumber() {
 		return HouseNumber;
 	}
 	public void setHouseNumber(List<Attribute> HouseNumber) {
 		this.HouseNumber = HouseNumber;
 	}
 	
 	private List<Attribute> Street;
 	public List<Attribute> getStreet() {
 		return Street;
 	}
 	public void setStreet(List<Attribute> Street) {
 		this.Street = Street;
 	}
 	
 	private List<Attribute> Street3;
 	public List<Attribute> getStreet3() {
 		return Street3;
 	}
 	public void setStreet3(List<Attribute> Street3) {
 		this.Street3 = Street3;
 	}
 	
 	private List<Attribute> District;
 	public List<Attribute> getDistrict() {
 		return District;
 	}
 	public void setDistrict(List<Attribute> District) {
 		this.District = District;
 	}
 	
 	private List<Attribute> Region;
 	public List<Attribute> getRegion() {
 		return Region;
 	}
 	public void setRegion(List<Attribute> Region) {
 		this.Region = Region;
 	}
 	
 	private List<Attribute> Source;
 	public List<Attribute> getSource() {
 		return Source;
 	}
 	public void setSource(List<Attribute> Source) {
 		this.Source = Source;
 	}
 	
 	private List<Attribute> CleanserHouseNumber;
 	public List<Attribute> getCleanserHouseNumber() {
 		return CleanserHouseNumber;
 	}
 	public void setCleanserHouseNumber(List<Attribute> CleanserHouseNumber) {
 		this.CleanserHouseNumber = CleanserHouseNumber;
 	}
 	
 	private List<Attribute> CleanserAddressLine1;
 	public List<Attribute> getCleanserAddressLine1() {
 		return CleanserAddressLine1;
 	}
 	public void setCleanserAddressLine1(List<Attribute> CleanserAddressLine1) {
 		this.CleanserAddressLine1 = CleanserAddressLine1;
 }	
 	
 	private List<Attribute> CleanserAddressLine2;
 	public List<Attribute> getCleanserAddressLine2() {
 		return CleanserAddressLine2;
 	}
 	public void setCleanserAddressLine2(List<Attribute> CleanserAddressLine2) {
 		this.CleanserAddressLine2 = CleanserAddressLine2;
 	}
 
 	private List<Attribute> CleanserStreet;
 	public List<Attribute> getCleanserStreet() {
 		return CleanserStreet;
 	}
 	public void setCleanserStreet(List<Attribute> CleanserStreet) {
 		this.CleanserStreet = CleanserStreet;
 	}
 	
 	private List<Attribute> CleanserSubBuilding;
 	public List<Attribute> getCleanserSubBuilding() {
 		return CleanserSubBuilding;
 	}
 	public void setCleanserSubBuilding(List<Attribute> CleanserSubBuilding) {
 		this.CleanserSubBuilding = CleanserSubBuilding;
 	}
 	
 	private List<Attribute> CleanserPOBox;
 	public List<Attribute> getCleanserPOBox() {
 		return CleanserPOBox;
 	}
 	public void setCleanserPOBox(List<Attribute> CleanserPOBox) {
 		this.CleanserPOBox = CleanserPOBox;
 	}
 	
 	private List<Attribute> CleanserCity;
 	public List<Attribute> getCleanserCity() {
 		return CleanserCity;
 	}
 	public void setCleanserCity(List<Attribute> CleanserCity) {
 		this.CleanserCity = CleanserCity;
 	}
 	
 	private List<Attribute> CleanserRegion;
 	public List<Attribute> getCleanserRegion() {
 		return CleanserRegion;
 	}
 	public void setCleanserRegion(List<Attribute> CleanserRegion) {
 		this.CleanserRegion = CleanserRegion;
 	}
 
 	private List<Attribute> CleanserCountry;
 	public List<Attribute> getCleanserCountry() {
 		return CleanserCountry;
 	}
 	public void setCleanserCountry(List<Attribute> CleanserCountry) {
 		this.CleanserCountry = CleanserCountry;
 	}
 	
 	private List<CleanserPostalCode> CleanserPostalCode;
 	public List<CleanserPostalCode> getCleanserPostalCode() {
 		return CleanserPostalCode;
 	}
 	public void setCleanserPostalCode(List<CleanserPostalCode> CleanserPostalCode) {
 		this.CleanserPostalCode = CleanserPostalCode;
 	}
 	
 	private List<CleanserGeoLocation> CleanserGeoLocation;
 	public List<CleanserGeoLocation> getCleanserGeoLocation() {
 		return CleanserGeoLocation;
 	}
 	public void setCleanserGeoLocation(List<CleanserGeoLocation> CleanserGeoLocation) {
 		this.CleanserGeoLocation = CleanserGeoLocation;
 	} 
}
