package com.bd.common.bo;

import java.util.List;

public class SalesOrganization {
	
	String label;
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	private SalesOrganizationAttributes attributes;
	public SalesOrganizationAttributes getAttributes() {
		return attributes;
	}
	public void setAttributes(SalesOrganizationAttributes attributes) {
		this.attributes = attributes;
	}

	private String type;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	private String uri;
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}

	private String createdTime;
	public String getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	private String updatedTime;
 	public String getUpdatedTime() {
 		return updatedTime;
 	}
 	public void setUpdatedTime(String updatedTime) {
 		this.updatedTime = updatedTime;
 	}

 	private String relationshipLabel;
 	public String getRelationshipLabel() {
 		return relationshipLabel;
 	}
 	public void setRelationshipLabel(String relationshipLabel) {
 		this.relationshipLabel = relationshipLabel;
 	}

 	private List<Crosswalk> crosswalks;
 	public List<Crosswalk> getCrosswalks() {
 		return crosswalks;
 	}
 	public void setCrosswalks(List<Crosswalk> crosswalks) {
 		this.crosswalks = crosswalks;
 	}

 	private List<Crosswalk> startObjectCrosswalks;
 	public List<Crosswalk> getStartObjectCrosswalks() {
		return startObjectCrosswalks;
	}
 	public void setStartObjectCrosswalks(List<Crosswalk> startObjectCrosswalks) {
		this.startObjectCrosswalks = startObjectCrosswalks;
	}
 
 	private List<Crosswalk> endObjectCrosswalks;
 	public List<Crosswalk> getEndObjectCrosswalks() {
		return endObjectCrosswalks;
	}
	public void setEndObjectCrosswalks(List<Crosswalk> endObjectCrosswalks) {
		this.endObjectCrosswalks = endObjectCrosswalks;
	}

	private Crosswalk refEntity;
	public Crosswalk getRefEntity() {
		return refEntity;
	}
	public void setRefEntity(Crosswalk refEntity) {
		this.refEntity = refEntity;
	}

	private Crosswalk refRelation;
	public Crosswalk getRefRelation() {
		return refRelation;
	}
	public void setRefRelation(Crosswalk refRelation) {
		this.refRelation = refRelation;
	}

	private SalesOrganizationAttributes value;
	public SalesOrganizationAttributes getValue() {
		return value;
	}
	public void setValue(SalesOrganizationAttributes value) {
		this.value = value;
	}
 
	public Boolean ov;
	public Boolean getOv() {
		return ov;
	}
	public void setOv(Boolean ov) {
		this.ov = ov;
	}
	public Boolean isOv() {
		return ov;
	}
}
