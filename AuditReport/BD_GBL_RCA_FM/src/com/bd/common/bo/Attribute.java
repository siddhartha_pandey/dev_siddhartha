package com.bd.common.bo;

import java.util.List;

public class Attribute {

	public String type;
	public String value;
	public Boolean dataProvider;
	public String createDate;
	public String updateDate;
	public String deleteDate;
	public String lookupCode;
	public Boolean ov;
	public String uri;
	private List<String> attributes;
	
	public List<String> getAttributes() {
		return attributes;
	}
	public void setAttributes(List<String> attributes) {
		this.attributes = attributes;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public Boolean getOv() {
		return ov;
	}
	public String getType() {
		return type;
	}
	public String getLookupCode() {
		return lookupCode;
	}
	public void setLookupCode(String lookupCode) {
		this.lookupCode = lookupCode;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Boolean getDataProvider() {
		return dataProvider;
	}
	public void setDataProvider(Boolean dataProvider) {
		this.dataProvider = dataProvider;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getDeleteDate() {
		return deleteDate;
	}
	public void setDeleteDate(String deleteDate) {
		this.deleteDate = deleteDate;
	}
	public Boolean isOv() {
		return ov;
	}
	public void setOv(Boolean ov) {
		this.ov = ov;
	}
	
}
