package com.bd.common.bo;

import java.util.List;

public class SalesOrganizationAttributes {
	private List<Attribute> SalesOrganizationID;
	public List<Attribute> getSalesOrganizationID() {
		return SalesOrganizationID;
	}
	public void setSalesOrganizationID(List<Attribute> salesOrganizationID) {
		SalesOrganizationID = salesOrganizationID;
	}

	private List<Attribute> Description;
	public List<Attribute> getDescription() {
		return Description;
	}
	public void setDescription(List<Attribute> description) {
		this.Description = description;
	}
}