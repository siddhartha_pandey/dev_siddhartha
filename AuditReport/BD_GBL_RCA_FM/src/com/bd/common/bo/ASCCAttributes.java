package com.bd.common.bo;

import java.util.List;

public class ASCCAttributes {
	private List<Attribute> MarketSegmentOverride;
	public List<Attribute> getMarketSegmentOverride() {
		return MarketSegmentOverride;
	}
	public void setMarketSegmentOverride(List<Attribute> marketSegmentOverride) {
		MarketSegmentOverride = marketSegmentOverride;
	}
}
