package com.bd.common.bo;

import java.util.List;

public class ReltioObject {

	private CustomerAttributes attributes;
    public CustomerAttributes getCustomerAttributes() {
        return attributes;
    }
    public void setCustomerAttributes(CustomerAttributes Attributes) {
        this.attributes = Attributes;
    }

    private String type;
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    private String uri;
    public String getUri() {
        return uri;
    }
    public void setUri(String uri) {
        this.uri = uri;
    }

    private String createdTime;
    public String getCreatedTime() {
        return createdTime;
    }
    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    private String updatedTime;
    public String getUpdatedTime() {
        return updatedTime;
    }
    public void setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
    }

    private String relationshipLabel;
    public String getRelationshipLabel() {
        return relationshipLabel;
    }
    public void setRelationshipLabel(String relationshipLabel) {
        this.relationshipLabel = relationshipLabel;
    }

    private List<Crosswalk> crosswalks;
    public List<Crosswalk> getCrosswalks() {
        return crosswalks;
    }
    public void setCrosswalks(List<Crosswalk> crosswalks) {
        this.crosswalks = crosswalks;
    }

    private Crosswalk refEntity;
    public Crosswalk getRefEntity() {
        return refEntity;
    }
    public void setRefEntity(Crosswalk refEntity) {
        this.refEntity = refEntity;
    }

    private Crosswalk refRelation;
    public Crosswalk getRefRelation() {
        return refRelation;
    }
    public void setRefRelation(Crosswalk refRelation) {
        this.refRelation = refRelation;
    }
	
}
