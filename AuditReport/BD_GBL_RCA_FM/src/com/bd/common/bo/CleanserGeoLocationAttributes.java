package com.bd.common.bo;

import java.util.List;

public class CleanserGeoLocationAttributes {
    private List<Attribute> CleanserLatitude;
    public List<Attribute> getCleanserLatitude() {
        return CleanserLatitude;
    }
    public void setLatitude(List<Attribute> CleanserLatitude) {
        this.CleanserLatitude = CleanserLatitude;
    }
    
    private List<Attribute> CleanserLongitude;
    public List<Attribute> getCleanserLongitude() {
        return CleanserLongitude;
    }
    public void setCleanserLongitude(List<Attribute> CleanserLongitude) {
        this.CleanserLongitude = CleanserLongitude;
    }
    
    private List<Attribute> CleanserGeoAccuracy;
    public List<Attribute> getCleanserGeoAccuracy() {
        return CleanserGeoAccuracy;
    }
    public void setCleanserGeoAccuracy(List<Attribute> CleanserGeoAccuracy) {
        this.CleanserGeoAccuracy = CleanserGeoAccuracy;
    }
}
